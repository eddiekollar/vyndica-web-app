Template['profile'].onCreated(function () {
  this.readonly = new ReactiveVar(true)
})

Template['profile'].onRendered(function () {
  const state = (this.data.profile && this.data.profile.state) ? this.data.profile.state : ''
  $('[data-schema-key=state]').val(state)
})

Template['profile'].helpers({
  getSchema: function () {
    return Profiles.simpleSchema()
  },
  getDoc: function () {
    return Profiles.findOne({userId: Meteor.userId()})
  },
  getFormType: function () {
    const type = (Template.instance().readonly.get()) ? 'readonly' : 'method-update'
    return type
  },
  getCollection: function () {
    return Profiles
  }
})

Template['profile'].events({
  'click #editBtn': function (event, template) {
    event.preventDefault()
    template.readonly.set(!template.readonly.get())
  }

})

AutoForm.addHooks('viewEditUserProfile', {
  onSubmit: function (insertDoc, updateDoc, currentDoc) {
    console.log(insertDoc, updateDoc, currentDoc)
    this.done()
  } /*
  before: {
    'method-update': function (modifier) {
      console.log(modifier)
      return modifier
    }
  }*/
})
