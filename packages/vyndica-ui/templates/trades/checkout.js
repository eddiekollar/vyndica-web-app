let isBraintreeInitialized = false

let initializeBraintree = (clientToken) => {
  if (isBraintreeInitialized) return

  braintree.setup(clientToken, 'dropin', {
    container: 'payment-form',
    paymentMethodNonceReceived: function (event, nonce) {
      console.log('nonce: ', nonce)

      // we've received a payment nonce from braintree
      // we need to send it to the server, along with any relevant form data
      // to make a transaction

      // enable submit?

    /*
    var data = serializeForm($('#checkout'))
    data.nonce = nonce

    Meteor.call('createTransaction', data, function (err, result) {
      Session.set('paymentFormStatus', null)
      Router.go('confirmation')
    })
    */
    }
  })

  isBraintreeInitialized = true
}

let getStep = () => {
  return Wizard.get('checkout').activeStep()['id']
}

/*
    Template.checkout.onRendered(function () {
      const state = (this.data.profile && this.data.profile.state) ? this.data.profile.state : ''
      $('[data-schema-key=state]').val(state)

    Meteor.call('getClientToken', '', function (err, clientToken) {
      if (err) {
        console.log('There was an error', err)
        return
      }

      initializeBraintree(clientToken)
    })

    })

    Template.checkout.onCreated(function () {
      Session.setDefault('hasError', false)
      const hasError = (Session.get('hasError') !== undefined) ? Session.get('hasError') : false
      this.data.hasError = new ReactiveVar(hasError)
    })

    Template.checkout.helpers({
      hasError: function () {
        return (Template.instance().data.hasError) ? Template.instance().data.hasError.get() : false
      }
    })
*/

Template.checkout.events({
  'click [data-action=confirm]': function (event, template) {
    if (getStep() === 'shipping-info') {
      $('#information-form').form({
        fields: {
          firstName: 'empty',
          lastName: 'empty',
          street1: 'empty',
          city: 'empty',
          state: 'empty',
          zip: 'empty'
        }
      })
      $('.ui.dimmer').dimmer('show')

      let doc = AutoForm.getFormValues('information-form').updateDoc.$set
      doc.name = doc.firstName + ' ' + doc.lastName

      Meteor.call('verifyAddress', doc, function (error, result) {
        $('.ui.dimmer').dimmer('hide')
        if (error) {
          // deal with it
          console.error('error')
          Session.set('hasError', true)
        } else {
          Session.set('hasError', !result)
          if (result) {
            Wizard.get('checkout').next()
          }
        }
      })
    } else {
      // Submit
      var shippingInfo = _.reduce($('#information-form').serializeArray(), function (memo, prop) {
        obj = {}
        obj[prop.name] = prop.value
        return _.extend(memo, obj)
      }, {})
    }
  }
})

Template.checkout.helpers({
  currentStep: function () {
    return getStep()
  },
  steps: function () {
    return [{
      id: 'shipping-info',
      title: 'Shipping Information',
      formId: 'information-form',
      template: 'shipping-information',
      schema: Schemas.profile,
      data: Template.instance().data.profile
    }, {
      id: 'payment',
      title: 'Payment',
      formId: 'interests-form',
      template: 'payment-information',
      onSubmit: (data, wizard) => {
        $('.ui.text.loader').text('Confirming trade...')

        const tradeEdgeId = Session.get('tradeEdgeId')

        Meteor.call('confirmTradeEdge', tradeEdgeId, result.addressId, function (error, result) {
          if (error) {
            console.error(error)
            $('.ui.dimmer').dimmer('hide')
          } else {
            $('.ui.dimmer').dimmer('hide')
            $('.ui.modal.checkout').modal('hide')

            Session.set('popupMessage', {type: 'confirm', message: ''})
            $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true}).modal('show')
          }
        })
      }
    }]
  }
})

Template['shipping-information'].onCreated(function () {
  Session.setDefault('hasError', false)
})

Template['shipping-information'].onRendered(function () {
  const state = (this.data.profile && this.data.profile.state) ? this.data.profile.state : ''
  $('[data-schema-key=state]').val(state)
})

Template['shipping-information'].helpers({
  hasError: function () {
    return Session.get('hasError')
  }
})

Template['payment-information'].onRendered(function () {
  Meteor.call('getClientToken', '', function (err, clientToken) {
    if (err) {
      console.log('There was an error', err)
      return
    }

    initializeBraintree(clientToken)
  })
})
