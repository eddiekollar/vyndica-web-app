Template.looplist.onCreated(function () {
  const tradeloop = this.data.tradeloop
  const edges = this.data.tradeloop.tradeEdgeIds

  this.userPairs = new ReactiveVar([])
  this.otherPairs = new ReactiveVar([])

  let self = this
  Tracker.autorun(function () {
    if (tradeloop.status === 'cancelled' || tradeloop.status === 'confirmed') {
      let userPairs = []
      let otherPairs = []
      _.each(edges, function (edge) {
        const ids = edge.split(':')
        const userItem = Items.findOne({_id: ids[0]})
        const ownerId = (userItem && userItem.ownerId) ? userItem.ownerId : ''

        const doc = {
          _id: edge,
          divId: edge.replace(':', ''),
          userItem: userItem,
          tradeItem: Items.findOne({_id: ids[1]}),
          ownerId: ownerId
        }
        if (doc.ownerId === Meteor.userId()) {
          userPairs.push(doc)
        } else {
          otherPairs.push(doc)
        }
      })
      self.userPairs.set(userPairs)
      self.otherPairs.set(otherPairs)
    } else {
      self.userPairs.set(self.data.usersPairs)
      self.otherPairs.set(self.data.otherPairs)
    }
  })
})

Template.looplist.helpers({
  usersPairs: function () {
    return Template.instance().userPairs.get()
  },
  otherPairs: function () {
    return Template.instance().otherPairs.get()
  },
  getProfile: function () {
    return Profiles.findOne({userId: Meteor.userId()})
  }
})

Template.looptradepair.events({
  'click [data-action=confirm]': function (event, template) {
    // Session.set('popupMessage', {type: 'warn', message: 'You are about to confirm this trade'})
    Session.set('tradeEdgeId', template.data._id)
    $('.ui.modal.checkout').modal({detachable: false, observeChanges: true}).modal('show')
  },
  'click [data-action=cancel]': function (event, template) {
    Session.set('popupMessage', {type: 'warn', message: 'You are about to cancel this trade. Are you sure you want to make every a sad panda?'})
    $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true,
      onApprove: function () {
        // Cancel trade
        Meteor.call('cancelTradeEdge', template.data._id, template.data.tradecycleId, function (error, result) {
          if (error) {
            console.error(error)
          }
        })
      }
    }).modal('show')
  }
})

Template.checkout.onCreated(function () {
  Session.setDefault('hasError', false)
  const hasError = (Session.get('hasError') !== undefined) ? Session.get('hasError') : false
  this.data.hasError = new ReactiveVar(hasError)
})

Template.checkout.onRendered(function () {
  const state = (this.data.profile && this.data.profile.state) ? this.data.profile.state : ''
  $('[data-schema-key=state]').val(state)
})

Template.checkout.helpers({
  hasError: function () {
    return (Template.instance().data.hasError) ? Template.instance().data.hasError.get() : false
  }
})

Template.checkout.events({
  'click [data-action=confirm]': function (event, template) {
    $('#information-form').form({
      fields: {
        firstName: 'empty',
        lastName: 'empty',
        street1: 'empty',
        city: 'empty',
        state: 'empty',
        zip: 'empty'
      }
    })
    $('.ui.dimmer').dimmer('show')

    // check if valid
    let doc = AutoForm.getFormValues('information-form').updateDoc.$set
    doc.name = doc.firstName + ' ' + doc.lastName

    Meteor.call('verifyAddress', doc, function (error, result) {
      if (error) {
        // deal with it
        console.error('error')
        $('.ui.dimmer').dimmer('hide')
        template.data.hasError.set(true)
      } else {
        if (result) {
          $('.ui.text.loader').text('Confirming trade...')

          const tradeEdgeId = Session.get('tradeEdgeId')

          const info = {
            tradeEdgeId: tradeEdgeId,
            nonce: '',
            addressId: result.addressId
          }

          Meteor.call('confirmTradeEdge', info, function (error, result) {
            if (error) {
              console.error(error)
              $('.ui.dimmer').dimmer('hide')
            } else {
              $('.ui.dimmer').dimmer('hide')
              $('.ui.modal.checkout').modal('hide')

              Session.set('popupMessage', {type: 'confirm', message: ''})
              $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true}).modal('show')
            }
          })

          template.data.hasError.set(false)
        } else {
          // deal with it
          console.error('invalid')
          $('.ui.dimmer').dimmer('hide')
          template.data.hasError.set(true)
        }
      }
    })
  }
})
