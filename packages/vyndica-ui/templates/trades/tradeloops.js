Template.tradeloop.onCreated(function () {
  let tradepairs = []

  const _id = this.data._id
  const status = this.data.status
  _.each(this.data.tradeEdgeIds, function (tradeEdgeId) {
    const ids = tradeEdgeId.split(':')
    const item = Items.findOne({_id: ids[0], ownerId: Meteor.userId()})

    if (item) {
      const traidpair = {
        userItem: item,
        tradeItem: Items.findOne({_id: ids[1]}),
        tradecycleId: _id,
        status: status
      }
      tradepairs.push(traidpair)
    }
  })

  this.tradepairs = new ReactiveVar(tradepairs)
})

Template.tradeloop.helpers({
  getTradePairs: function () {
    return Template.instance().tradepairs.get()
  }
})

Template.tradeloop.events({
  'click .tradepair.item': function (event, template) {
    Router.go('loop.show', {_id: event.target.id})
  }
})
