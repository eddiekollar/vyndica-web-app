Template['items'].onCreated(function () {
  var searchTerm = $('#search').val()
  ItemsSearch.search(searchTerm)
})

Template['items'].onRendered(function () {
  $('a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash)
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000)
        return false
      }
    }

    return true
  })

  var $grid = $('.items-grid').imagesLoaded(function () {
    $grid.masonry({
      itemSelector: '.grid-item',
      percentPosition: true,
      columnWidth: '.grid-sizer'
    })
  })
})

Template['items'].helpers({
  getItems: function () {
    return (Router.current().route.getName() === 'search') ? ItemsSearch.getData() : Template.instance().data.items
  }
})

Template['items'].events({

})
