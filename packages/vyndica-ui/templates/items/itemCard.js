Template.itemCard.onCreated(function () {
  this.itemIsWanted = new ReactiveVar(this.data.isWanted)
})

Template.itemCard.onRendered(function () {
  $('.ui.star').rating()
  $('.ui.dropdown').dropdown()
})

Template['itemCard'].helpers({
  getImgURL: function () {
    var item = Items.findOne({_id: this._id})
    var url = (item.img && item.img.url) ? item.imgURL('h_200,w_200,c_fit,q_70') : 'https://placeholdit.imgix.net/~text?txtsize=35&txt=No+Image&w=200&h=200'
    return url
  },
  getImg: function () {
    return $.cloudinary.image(this._id + '.jpg', { width: 200, height: 200, crop: 'fit', quality: 70})
  },
  itemIsWanted: function () {
    return Template.instance().itemIsWanted.get()
  },
  getData: function () {
    console.log(Template.instance())
  },
  getCommentCount: function () {
    return Comments._collection.find({referenceId: this._id}).count()
  }
})

Template['itemCard'].events({
  'click .ui.star': function (event) {
    console.log(event)
    console.log($('.ui.star').rating('get rating'))
  },
  'click .image': function (event) {
    Router.go('item.show', {_id: this._id})
  },
  'click #details': function (event) {
    Router.go('item.show', {_id: this._id})
  },
  'click [data-action=add-to-wants]': function (event, template) {
    template.data.isWanted = true
    template.itemIsWanted.set(true)
    console.log(template)
    Meteor.call('updateList', 'wants', {$push: {wants: template.data._id} }, function () {})
  },
  'click [data-action=remove-from-wants]': function (event, template) {
    template.data.isWanted = false
    template.itemIsWanted.set(false)

    Meteor.call('updateList', 'wants', {$pop: {wants: template.data._id} }, function () {})
  }
})
