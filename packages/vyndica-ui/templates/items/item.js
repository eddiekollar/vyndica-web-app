Template['item'].onRendered(function () {
  $('.ui.star').rating('disable')

  if (Meteor.user()) {
    Meteor.call('notificationRead', this.data.doc._id, function (error, result) {
      if (error) {
        // something
      }
    })
  }
})

Template['item'].onCreated(function () {
  this.readonly = new ReactiveVar(true)
  const img = this.data.doc.img

  const url = (img && img.url) ? this.data.doc.imgURL('h_350,w_250,c_fit,q_80') : 'https://placeholdit.imgix.net/~text?txtsize=35&txt=No+Image&w=250&h=250'
  this.imgSrc = new ReactiveVar(url)

  this.itemIsWanted = new ReactiveVar(this.data.doc.isWanted)

  this.status = new ReactiveVar()

  const self = this
  const itemId = this.data.doc._id

  Tracker.autorun(function () {
    self.status.set(Items.findOne({_id: itemId}).status)
  })
})

Template['item'].helpers({
  canDelete: function () {
    return Template.instance().status.get() === 'available'
  },
  canEdit: function () {
    return Template.instance().status.get() === 'available'
  },
  getImgSrc: function () {
    return Template.instance().imgSrc.get()
  },
  getFormType: function () {
    return (Template.instance().readonly.get()) ? 'readonly' : 'update'
  },
  getTagName: function (interestId) {
    var interest = Interests.findOne({_id: interestId})
    return (interest && interest.title) ? interest.title : ''
  },
  itemIsWanted: function () {
    return Template.instance().itemIsWanted.get()
  },
  isAvailable: function () {
    const status = Template.instance().status.get()
    return status === 'available' || status === 'incycle'
  }
})

Template['item'].events({
  'click img#primary-img': function (event, template) {
    var imgPath = template.data.doc.imgURL('q_80'); // $(e.currentTarget).data('image')

    if (template.readonly.get()) {
      if (imgPath) {
        sImageBox.open(imgPath, {
          animation: 'zoomIn'
        })
      }
    }
  },
  'click #editBtn': function (event, template) {
    template.readonly.set(!template.readonly.get())
  },
  'click #deleteBtn': function (event, template) {
    $('.small.modal.delete').modal({
      onApprove: function () {
        Meteor.call('deleteItem', template.data.doc._id, function (error, res) {
          if (error) {
          }
          if (template.data.doc.img && (template.data.doc.img._id && template.data.doc.img._id !== '')) {
            // cleanupImg(template.data.doc.img._id)
          }
        })
        Router.go('home')
      }
    }).modal('show')
  },
  'click [data-action=add-to-wants]': function (event, template) {
    template.data.doc.isWanted = true
    template.itemIsWanted.set(true)

    Meteor.call('updateList', 'wants', {$push: {wants: template.data.doc._id} }, function () {})
  },
  'click [data-action=remove-from-wants]': function (event, template) {
    template.data.doc.isWanted = false
    template.itemIsWanted.set(false)

    Meteor.call('updateList', 'wants', {$pull: {wants: template.data.doc._id} }, function () {})
  }
})

AutoForm.addHooks('viewEditItemForm', {
  /*onSubmit: function (insertDoc, updateDoc, currentDoc) {
      console.log(insertDoc, updateDoc, currentDoc)
      this.done()
      return false
  },
  formToModifier: function(modifier) { 
      console.log(modifier)
      
      console.log(modifier)
      return modifier
  },*/
  before: {
    update: function (modifier) {
      modifier.$set.img = this.currentDoc.img || {}
      _.each(modifier['$set']['tags'], function (tag) {
        var exists = Interests.findOne({_id: tag})
        if (!exists) {
          var newTag = Interests.insert({'title': tag})
          modifier['$set']['tags'][modifier['$set']['tags'].indexOf(tag)] = newTag
        }
      })
      return modifier
    }
  }
})
