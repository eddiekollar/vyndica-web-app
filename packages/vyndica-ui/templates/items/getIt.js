Template.getIt.onCreated(function () {})

var tradeWithNew = function (template) {
  var pendingActionObj = {
    action: 'createTradeEdge',
    data: {
      getItemId: template.data._id
    }
  }
  Session.set('pendingActionObj', pendingActionObj)
  Router.go('item.add')
}

Template.getIt.events({
  'click [data-action=get-item]': function (event, template) {
    if (Meteor.user()) {
      Session.set('getItemId', template.data.doc._id)

      if (template.data.doc.possibleTradesCount() > 0) {
        $('.ui.modal.items.totrade').modal({detachable: false, observeChanges: true,
          onVisible: function () {
            var $listGroup = $(this).closest('.list'),
              $checkbox = $listGroup.find('.checkbox')

            $checkbox.each(function () { $(this).checkbox('is checked')})
          },
          onDeny: function () {
            var showItemsList = Session.get('showItemsList')
            if (showItemsList) {
              tradeWithNew(template)
            }
          },
          onApprove: function () {
            // upstream items
            var showItemsList = Session.get('showItemsList')
            var pendingActionObj = {}
            if (showItemsList) {
              var itemIds = []
              var items = $('.list').find(':checked')
              _.each(items, function (item) {
                itemIds.push(item.id)
                $(item).checkbox('uncheck')
              })

              pendingActionObj = {
                action: 'createTradeEdges',
                data: {
                  getItemId: template.data.doc._id,
                  items: itemIds
                }
              }

              Session.set('pendingActionObj', pendingActionObj)
              $('.fluid.ui.approve').removeClass('disabled')
              Session.set('showItemsList', false)
            } else {
              var items = $('.tradepair.item')

              _.each(items, function (item) {
                var idField = $(item).find('#id')[0]
                var id = $(idField).text().split(':')

                Meteor.call('insertTradeEdge', id[0], id[1], function (error, result) {
                  if (error) {
                    console.log('insertTradeEdge error: ', error)
                  } else {
                    console.log('insertTradeEdge: ', result)
                  }
                })
              })
              Session.set('popupMessage', {type: 'confirm', message: ''})
              $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true}).modal('show')
            }

            return !showItemsList
          },
          onHidden: function () {
            Session.set('showItemsList', true)
          }
        }).modal('show').modal('refresh')
      } else {
        // Modal you don't have any items to trade for
        Session.set('popupMessage', {type: 'warn', message: '<p>Oops! No trades available. One of two things happened: <ol> <li> You have nothing listed to trade for this item.</li> <li> You have already proposed trades with all of your available items.</li></ol> List new items if you wish to make more trade proposals.</p>'})
        $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true,
          onApprove: function () {
            tradeWithNew(template)
          }
        }).modal('show')
      }
    } else {
      // Modal let's add a new item
      Session.set('popupMessage', {type: 'warn', message: "You don't have any items to trade. Let's add one."})
      $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true,
        onApprove: function () {
          tradeWithNew(template)
        }
      }).modal('show')
    }
  }
})
