Template['newItem'].onRendered(function () {
  $('select').dropdown({
    allowAdditions: true
  })
})

Template['newItem'].helpers({

})

Template['newItem'].events({
  'click img': function (event, template) {},
  'click input[type=submit]': function (event, template) {
    console.log('clicking on submit')
    event.preventDefault()
    $('#newItemForm').form({
      on: 'blur',
      fields: {
        title: 'empty',
        description: 'empty',
        city: 'empty',
        state: 'empty',
        zip: 'empty'
      }
    })

    var imgObj = template.data.doc.img

    if (!imgObj) {
      $('.small.modal.picture').modal('show')
      return
    }

    var doc = AutoForm.getFormValues('newItemForm').updateDoc.$set
    doc.img = imgObj
    var pendingActionObj = Session.get('pendingActionObj')

    if (Meteor.userId) {
      console.log(doc)

      _.each(doc.tags, function (tag) {
        var exists = Interests.findOne({_id: tag})
        console.log('Some tag: ', exists)
        if (!exists) {
          var newTag = Interests.insert({'title': tag})
          doc.tags[doc.tags.indexOf(tag)] = newTag
        }
      })

      Meteor.call('insertItem', doc, function (error, result) {
        if (error) {
          console.error(error)
        } else {
          console.log(result)

          if (pendingActionObj && pendingActionObj.action === 'createTradeEdge' && pendingActionObj.data && pendingActionObj.data.getItemId) {
            Meteor.call('insertTradeEdge', result, pendingActionObj.data.getItemId, function (err, res) {
              if (err) {
                console.error('insertTradeEdge error: ', err)
                Router.go('home')
              } else {
                console.log('insertTradeEdge: ', res)
                Session.set('popupMessage', {type: 'confirm', message: ''})
                $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true,
                  onApprove: function () {
                    Router.go('home')
                  }
                }).modal('show')
              }
              Session.set('pendingActionObj', {})
            })
          } else {
            Session.set('popupMessage', {type: 'confirm', message: ''})
            $('.ui.small.modal.confirm').modal({detachable: false, observeChanges: true,
              onApprove: function () {
                Router.go('home')
              }
            }).modal('show')
          }
        }
      })
    } else {
      // create session object
      var newPendingActionObj = {
        action: 'createTradeEdgeNewItem',
        data: {
          getItemId: newPendingActionObj.data.getItemId,
          item: doc
        }
      }

      Session.set('pendingActionObj', newPendingActionObj)
    // Signup process
    }
  }
})

/*
AutoForm.hooks({
    newItemForm: {
        before: {
            insert: function(doc){
                doc.img = this.currentDoc.img

                if(!doc.img) {
                    console.log('not image found')
                    $('.small.modal.picture').modal('show')
                    return false
                }

                _.each(doc.tags, function(tag) {
                    var exists = Interests.findOne({_id: tag})
                    console.log('Some tag: ', exists)
                    if(!exists) {
                        var newTag = Interests.insert({'title': tag})
                        doc.tags[doc.tags.indexOf(tag)] = newTag
                    }
                })
                
                return doc
            }
        },
        onSuccess: function(formType, result) {
            Router.go('home')
        },
        formToDoc: function(doc) {
            
        }
    }
})
*/
