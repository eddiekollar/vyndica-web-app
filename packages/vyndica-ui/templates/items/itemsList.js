Template['itemsList'].events({
  'click tr.clickable': function (event, template) {
    Router.go('item.show', {_id: event.target.id})
  }
})

Template.listItem.onCreated(function () {})
