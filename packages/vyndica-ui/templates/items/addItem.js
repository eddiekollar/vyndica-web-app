itemImageSchema = new SimpleSchema({
  img: {
    type: Object,
    optional: true
  }
});

itemQuantitySchema = new SimpleSchema({
  quantity: {
      type: Number,
      min: 1
    }
});

itemInfoSchema = new SimpleSchema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  zip: {
    type: String
  }
});

itemConditionSchema = new SimpleSchema({
  condition: {
    type: String,
    optional: true
  },
  conditionType: {
    type: String,
    optional: true
  } 
});


itemTagsSchema = new SimpleSchema({
  tags: {
    type: [String],
    optional: true,
    autoform: {
      type: "select",
      afFieldInput: {
        class: 'fluid search dropdown',
        multiple: true,
        allowAdditions: true,
        fullTextSearch: true
      },
      options: function() {
        var list = Interests.find().fetch();
        var options = [];

        list.forEach(function (interest) {
          options.push({label: interest.title, value: interest._id});
        });
        return options;
      }
    }
  }
});


Template.addItem.onRendered(function() {
  $('[type=button]').hide();
});

Template.addItem.onCreated(function(){
  Session.set('condition', '');
  Session.set('conditionType', '');
});

var processPendingAction = function(itemId){
  var pendingActionObj = Session.get('pendingActionObj');

  if(pendingActionObj && pendingActionObj.hasOwnProperty('action')) {
    if(pendingActionObj.action === '') {
      //pendingActionObj.getItemId
    }
  }
}

Template.addItem.helpers({
    'photo': function(){
        return Session.get('photo');
    },
    steps: function() {
      return [
      {
        id: 'picture',
        title: 'Add a Picture',
        template: 'itemPicture',
        schema: itemImageSchema
      },
      {
        id: 'information',
        title: 'Item Information',
        formId: 'information-form',
        template: 'itemInformation',
        schema: itemInfoSchema
      },
      {
        id: 'tags',
        title: 'Tag Item',
        formId: 'tags-form',
        template: 'itemTags',
        schema: itemTagsSchema
      },
      {
        id: 'quantity',
        title: 'Quantity',
        formId: 'quantity-form',
        template: 'itemQuantity',
        schema: itemQuantitySchema
      },
      {
        id: 'condition',
        title: 'Condition',
        formId: 'condition-form',
        template: 'itemCondition',
        schema: itemConditionSchema,
        onSubmit: function(data, wizard) {
          var itemData = wizard.mergedData();
          itemData.condition = Session.get('condition');
          itemData.conditionType = Session.get('conditionType');
          itemData.ownerId = Meteor.userId();

          console.log(itemData);

          photo = Session.get('photo');

          var byteString = atob(photo.split(',')[1]);

          // separate out the mime component
          var mimeString = photo.split(',')[0].split(':')[1].split(';')[0];
          console.log('mimeString: ', mimeString);
          console.log('length: ', byteString.length);

          var byteNumbers =  new ArrayBuffer(byteString.length);

          for (var i = 0; i < byteString.length; i++) {
            byteNumbers[i] = byteString.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);
          var blob = new Blob([byteArray], {type: mimeString});

          Cloudinary._upload_file(photo, {
            folder: ''
          }, function(err, res) {
            if(err) {
                console.log("Upload Error: ", err);
            }else {
              img = {};
              img.res  = res;
              console.log('res from cloudinary: ', res);
              img._id = res.public_id;
              img.url = res.url;
              
              itemData.img = img;
              Meteor.call('insertItem', itemData, function(err, result) {
                if(err) {
                  console.log(err);
                }else{
                  console.log(result);

                }
              });
            }
            //$('.ui.dimmer').dimmer('hide');
            //$('input[type=submit]').prop("disabled",false);
            
          });
          
        }
      }];
    }
});

Template.itemInformation.onRendered(function() {
  $('[type=button]').hide();
});

Template.itemTags.onRendered(function() {
  $('select').dropdown({
      allowAdditions: true
    });
  $('[type=button]').hide();
});


Template.itemQuantity.onRendered(function() {
  $('[type=button]').hide();
});

Template.itemCondition.onCreated(function() {
  this.showTypes = new ReactiveVar(false);
});

Template.itemCondition.helpers({
  showTypes: function () {
    console.log(Template.instance());
    return Template.instance().showTypes.get();
  },
  showButton: function() {
    var isUsed = (Session.get('condition') === 'used') ? (Session.get('conditionType')) : false;
    var showButton = (Session.get('condition') === 'new') || isUsed;

    return showButton;
  }
});

Template.itemCondition.events({
  'click .icon': function (event, template) {
    if(event.target.id === 'new') {
      template.showTypes.set(false);
      Session.set('condition', 'new');
    }else if(event.target.id === 'used') {
      template.showTypes.set(true); 
      Session.set('condition', 'used');
    }else{
      Session.set('conditionType', event.target.id);
    }
  },
  'click [data-action=used]': function (event, template) {
    console.log(template);
    template.showTypes.set(true);
  }
});

Template.itemCondition.onRendered(function() {
  $('[type=button]').hide();
});

Template.itemPicture.onRendered(function() {
  console.log(this);
});

Template.itemPicture.helpers({
  'photo': function(){
    return Session.get('photo');
  }
});

Template.addItem.onRendered(function() {
  Session.set('headerTitle', 'Add Listing');
});


Template.addItem.events({
  'click .capture': function () {
    MeteorCamera.getPicture({}, function(error, data){
          Session.set('photo', data);
        });
  }
});

/*
id: 'interests',
        title: 'Interests',
        formId: 'interests-form',
        template: 'interests',
        schema: Schemas.interests,
        onSubmit: function(data, wizard) {
          var profileData = wizard.mergedData();
          profileData.userId = Meteor.userId();
          console.log('data: ', data);
          console.log('profileData: ', profileData);
          var checked = $('[type="checkbox"]:checked');
          var interests = [];

          _.each(checked, function(elem) {
              interests.push(elem.value);
          });

          profileData.interests = interests;

          delete profileData.insertDoc;
          delete profileData.updateDoc;
          
          Meteor.call('createUserProfile', profileData, function(err, res) { 
            if(err) {
              console.error('createUserProfile error: ', err);
            }else{
              console.log('createUserProfile success');

              Router.go('home');
          }
          });
*/

/*
Template.onboard.rendered = function () {
    $('.ui.checkbox').checkbox();
};

Schemas.interests = new SimpleSchema({
    'interests': {
        type: [String],
        optional: true
    }
});

Template.information.onRendered(function(){
    $('nav').append('<button type="button" class="wizard-skip-button ui button">Skip</button>');
});

Template.interests.onRendered(function() {
    $('nav').append('<button type="button" class="wizard-skip-button ui button">Skip</button>');
});

Template.information.events({
    'click .wizard-skip-button': function(event, template) {
        event.preventDefault();        
        Router.go('home');
    }
});

Template['interests'].events({
    'click .submit.button': function(event, template) {
        console.log('clicking submit');
        event.preventDefault();        

       // Meteor.users.update( { _id: Meteor.userId() }, { $set: { 'profile.setup': true, 'profile.interests': interests }} );
    },
    'click .wizard-skip-button': function(event, template) {
        event.preventDefault();        
        Router.go('home');
    },
    'click .wizard-submit-button': function(event, template) {
        console.log('clicking confirm');
        //event.preventDefault();
        $('#information-form').form({
            fields: {
                firstName: 'empty',
                lastName: 'empty',
                street1: 'empty',
                city: 'empty',
                state: 'empty',
                zip: 'empty'
            }
        });
    }
});
*/