/*
Template.itemDetails.helpers({
    getCommentCount: function() {
        return Comments._collection.find({referenceId: this._id}).count();
    }
});
*/

Template.itemDetails.onCreated(function() {
    this.readonly = new ReactiveVar( true );
    var img = this.data.doc.img;

    var url = (img && img.url) ? this.data.doc.imgURL('h_250,w_250,c_fit') : 'https://placeholdit.imgix.net/~text?txtsize=23&txt=&w=250&h=250';
    this.imgSrc = new ReactiveVar(url);

    this.itemIsWanted = new ReactiveVar(this.data.doc.isWanted);
});

Template.itemDetails.events({
    'click img#primary-img' : function(event, template) {
        var imgPath = template.data.doc.img.url; //$(e.currentTarget).data('image')

        if(template.readonly.get()) {
            if (imgPath) {
                sImageBox.open(imgPath, {
                    animation: 'zoomIn'
                });
            }
        }
    },
    'click #editBtn': function(event, template) {
        template.readonly.set(!template.readonly.get());
    },
    'click #deleteBtn': function(event, template) {
        $('.small.modal').modal({
            onApprove: function() {
                
                Meteor.call('deleteItem', template.data.doc._id, function(error, res) {
                    if(error) {

                    }
                    if(template.data.doc.img && (template.data.doc.img._id && template.data.doc.img._id !== '')) {
                        cleanupImg(template.data.doc.img._id); 
                    }
                });
                Router.go('home');
            }
        }).modal('show');
    },
    'click [data-action=add-to-wants]': function(event, template) {
        template.data.doc.isWanted = true;
        template.itemIsWanted.set(true);

        Meteor.call('updateList', 'wants', {$push: {wants: template.data.doc._id} }, function() {});
    },
    'click [data-action=remove-from-wants]': function(event, template) {
        template.data.doc.isWanted = false;
        template.itemIsWanted.set(false);

        Meteor.call('updateList', 'wants', {$pull: {wants: template.data.doc._id} }, function() {});
    }
});
Template.itemDetails.helpers({
    getImgSrc: function () {
        return Template.instance().imgSrc.get();
    },
    getFormType: function() {
        return (Template.instance().readonly.get()) ? 'readonly' : 'update';
    },
    getTagName: function(interestId) {
        var interest = Interests.findOne({_id: interestId});
        return (interest && interest.title) ? interest.title : '';
    },
    itemIsWanted: function() {
        return Template.instance().itemIsWanted.get();
    },
    isDocOwner: function() {
        return (this.doc && (Meteor.userId() === this.doc.ownerId));
    }
});