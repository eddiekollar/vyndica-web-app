Template.popupTrade.onCreated(function () {
  Session.setDefault('showItemsList', true)
})

Template.popupTrade.onRendered(function () {
  $('.ui.toggle.child.checkbox').checkbox({
    onChecked: function () {
      $('.fluid.ui.approve').removeClass('disabled')
    },
    onUnchecked: function () {
      var $listGroup = $(this).closest('.list'),
        $checkbox = $listGroup.find('.checkbox')

      var count = 0
      console.log('uncheck')

      $checkbox.each(function () {
        if ($(this).checkbox('is checked')) {
          count += 1
        }
      })

      if (count === 0) {
        $('.fluid.ui.approve').addClass('disabled')
      }
    }
  })
})

Template.tradeListItem.onRendered(function () {
  $('.ui.toggle.child.checkbox').checkbox({
    onChecked: function () {
      $('.fluid.ui.approve.positive.totrade').removeClass('disabled')
    },
    onUnchecked: function () {
      var $listGroup = $(this).closest('.list'),
        $checkbox = $listGroup.find('.checkbox')

      var count = 0
      $checkbox.each(function () {
        if ($(this).checkbox('is checked')) {
          count += 1
        }
      })

      if (count === 0) {
        $('.fluid.ui.approve.positive.totrade').addClass('disabled')
      }
    }
  })
})

/*
Originally used for single select

{
        onChecked: function() {
            var
            $listGroup      = $(this).closest('.list'),
            $checkbox       = $listGroup.find('.checkbox')

            var id = this.id
            $checkbox.each(function() {
                if( $(this).checkbox('is checked') && this.id !== id) {
                    $(this).checkbox('uncheck')
                }
            })
        }
    }
*/

Template.popupTrade.helpers({
  showItemsList: function () {
    return Session.get('showItemsList')
  }
})

Template.tradeList.onRendered(function () {})

Template.tradeList.helpers({
  usersItems: function () {
    var regexp = new RegExp(Session.get('getItemId') + '$', 'i')

    var incomingIds = _.pluck(Tradeedges.find({_id: {$regex: regexp}},
      {transform: function (doc) {
          // inItem is the item that makes up an incoming edge to the getItem
          doc.inItemId = doc._id.split(':')[0]
          return doc
        }
      }).fetch(), 'inItemId')

    return Items.find({ownerId: Meteor.userId(), _id: {$nin: incomingIds}, status: {$nin: ['unavailable', 'deleted']}})
  }
})

Template.tradeConfirm.helpers({
  tradePairs: function () {
    var list = []
    var pendingActionObj = Session.get('pendingActionObj')
    var tradeItem = Items.findOne({_id: pendingActionObj.data.getItemId})

    _.each(pendingActionObj.data.items, function (id, index) {
      divId = id + tradeItem._id
      tradePairId = id + ':' + tradeItem._id
      listItem = {
        _id: tradePairId,
        divId: divId,
        userItem: Items.findOne({_id: id}),
        tradeItem: tradeItem
      }

      list.push(listItem)
    })
    return list
  }
})

Template.tradepair.helpers({
  getStatus: function () {
    const status = Template.instance().data.status
    return (status) ? status : ''
  },
  getTradeLoopId: function () {
    const tradeloopId = Tradecycles.findOne({tradeEdgeIds: {$in: [Template.instance().data._id]}})._id
    return tradeloopId
  }
})

Template.tradepair.events({
  'click [data-action=gotoloop]': function (event, template) {
    const tradeloopId = Tradecycles.findOne({tradeEdgeIds: {$in: [this._id]}})._id
    Router.go('loop.show', {_id: tradeloopId})
  },
  'click [data-action=remove]': function (event, template) {
    var $div = $('div#' + event.target.id)

    var idField = $div.find('#id')[0]
    var id = $(idField).text()

    if (Router.current().route.getName() === 'items.want') {
      Meteor.call('deleteTradeedge', id, function (error, result) {
        if (error) {
          console.log('deleteTradeedge error: ', error)
        } else {
          console.log('deleteTradeedge: ', result)
          $div.remove()
        }
      })
    } else {
      $div.remove()
      if ($('.tradepair.item').size() === 0) {
        $('.fluid.ui.approve').addClass('disabled')
      }
    }
  }
})

Template.popupConfirm.onRendered(function () {})

Template.popupConfirm.helpers({
  isInfo: function () {
    var popupMsg = Session.get('popupMessage')
    return (popupMsg && popupMsg.type && popupMsg.type === 'info')
  },
  isConfirm: function () {
    var popupMsg = Session.get('popupMessage')
    return (popupMsg && popupMsg.type && popupMsg.type === 'confirm')
  },
  getContent: function () {
    var popupMsg = Session.get('popupMessage')
    return (popupMsg && popupMsg.message) ? popupMsg.message : ''
  }
})
