Package.describe({
  name: 'vyndica:ui',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
})

Package.onUse(function (api) {
  api.versionsFrom('1.2.1')
  api.use(['less',
    'ejson',
    'ecmascript',
    'blaze-html-templates',
    'templating',
    'reactive-var',
    'underscore',
    'accounts-password',
    'fastclick',
    'raix:handlebar-helpers',
    'fastclick',
    'zimme:iron-router-active',
    'useraccounts:semantic-ui',
    'natestrauser:animate-css',
    'dburles:collection-helpers',
    'spacebars',
    'semantic:ui-css',
    'yasinuslu:blaze-meta',
    'aldeed:autoform',
    'fabienb4:autoform-semantic-ui',
    'aldeed:autoform-select2',
    'yogiben:autoform-tags',
    'lepozepo:cloudinary',
    'bojicas:inflections',
    'aldeed:template-extension',
    'meteorhacks:search-source',
    'forwarder:autoform-wizard',
    'wylio:winston-papertrail',
    'juliancwirko:s-image-box',
    'voodoohop:masonrify',
    'okgrow:analytics',
    'goltfisch:hotjar',
    'arkham:comments-ui',
    'mdg:camera'])

  api.addFiles(['templates/common/wizard/steps.html',
    'templates/common/wizard/steps.js',
    'templates/items/item.html',
    'templates/items/item.js',
    'templates/items/itemDetails.html',
    'templates/items/itemDetails.js',
    'templates/items/items.html',
    'templates/items/items.js',
    'templates/items/newItem.html',
    'templates/items/newItem.js',
    'templates/items/itemCard.html',
    'templates/items/itemCard.js',
    'templates/items/itemsList.html',
    'templates/items/itemsList.js',
    'templates/items/getIt.html',
    'templates/items/getIt.js',
    'templates/items/addItem.html',
    'templates/items/addItem.js',
    'templates/items/userItems.html',
    'templates/items/userItems.js',
    'templates/items/popupItemsList.html',
    'templates/items/popupItemsList.js',
    'templates/trades/looplist.html',
    'templates/trades/looplist.js',
    'templates/trades/tradeloops.html',
    'templates/trades/tradeloops.js',
    'templates/profile/profile.html',
    'templates/profile/profile.js'],
    'client')

  /*

  */

  api.addAssets([
    'client/config.js',
    'client/lib/constants.js',
    'assets/favicon.ico',
    'assets/images/justen.jpg',
    'assets/images/logo.png',
    'assets/images/logo.jpg',
    'assets/images/getIt.png',
    'assets/images/items.png',
    'assets/images/giphy.gif'
  ], 'client')

  api.addAssets([
    'assets/fonts/basic.icons.eot',
    'assets/fonts/basic.icons.svg',
    'assets/fonts/basic.icons.ttf',
    'assets/fonts/basic.icons.woff',
    'assets/fonts/icons.eot',
    'assets/fonts/icons.otf',
    'assets/fonts/icons.svg',
    'assets/fonts/icons.ttf',
    'assets/fonts/icons.woff'
  ], 'client')

  // Cordova dependencies
  api.addAssets([], 'web.cordova')
})

/* No tests yet
Package.onTest(function(api) {
  api.use('ecmascript')
  api.use('tinytest')
  api.use('vyndica:ui')
  api.addFiles('ui-tests.js')
})
*/

Cordova.depends({
  'cordova-plugin-camera': '2.1.0'
})
