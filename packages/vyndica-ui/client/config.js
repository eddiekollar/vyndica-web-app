// AutoForm configs
AutoForm.setDefaultTemplate('semanticUI')
SimpleSchema.debug = true

// Wizard
Wizard.extendOptions(['wizardClasses'], {wizardClasses: 'grid ui'})

$.cloudinary.config({
  cloud_name: 'vyndica'
})

// Web mobile config for Meta and SEO
if (!Meteor.isCordova) {
  Meta.set([
    {
      name: 'name',
      property: 'mobile-web-app-capable',
      content: 'yes'
    }, {
      name: 'name',
      property: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }
  ])

  SEO.config({
    title: Meteor.App.NAME,
    meta: {
      'description': Meteor.App.DESCRIPTION
    }
  })
}
