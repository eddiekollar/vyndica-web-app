// Define App Constants

if (Meteor.App) {
  throw new Meteor.Error('Meteor.App already defined? see client/lib/constants.js');
}

Meteor.App = {
  NAME: 'Vyndica',
  DESCRIPTION: 'A group bartering site that algorithmically includes as many people needed to satisfy all trades.'
};
