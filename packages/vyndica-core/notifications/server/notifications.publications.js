Meteor.publishComposite('notifications', function(){
    return {
        find: function () {
            return Notifications.find({userId: this.userId});
        },
        children: [
            {
                find: function(notification) {
                    return Items.find({_id: notification.itemId});
                }
            }
        ]
    }
});