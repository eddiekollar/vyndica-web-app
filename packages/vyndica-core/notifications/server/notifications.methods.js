Meteor.methods({
    notificationRead: function(_id) {
        check(_id, String);
        return Notifications.remove({itemId: _id, userId: this.userId});
    }
});