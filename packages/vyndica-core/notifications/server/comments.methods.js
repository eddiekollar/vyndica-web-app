/* 

Over writting the comments/add method to include collection name

*/

Meteor.methods({
  'addCustomComment': function (referenceId, collectionName, content) {
    check(referenceId, String)
    check(collectionName, String)
    check(content, String)

    const userId = this.userId

    content = content.trim()


    if (userId && content) {
      Comments._collection.insert({
        referenceId: referenceId,
        content: content,
        userId: userId,
        collectionName: collectionName,
        createdAt: (new Date()),
        likes: [],
        replies: [],
        isAnonymous: false
      })
    }
  }
})
