var createUpdateNotifications = function (itemId) {
  NotificationSubscriptions.find({itemId: itemId}).forEach(function (subscription) {
    _.each(subscription.subscribers, function (subscriber) {
      if (subscriber !== this.userId) {
        Notifications.update({itemId: subscription.itemId, userId: subscriber}, {$set: {itemId: subscription.itemId, userId: subscriber}}, {upsert: true})
      }
    })
  })
}

Comments._collection.after.insert(function (userId, doc) {
  Items.update({_id: doc.referenceId}, {$set: {lastCommentedOn: new Date()}})
  if (doc.collectionName && doc.collectionName === 'items') {
    createUpdateNotifications(doc.referenceId)
  }else if (doc.collectionName && doc.collectionName === 'tradecycles') {
    // Some notification
  }
  sendCommentEmails(doc)
})

Comments._collection.before.update(function (userId, doc, fieldNames, modifier, options) {
  modifier.$set['updateAt'] = new Date()
})

Comments._collection.after.update(function (userId, doc, fieldNames, modifier, options) {
  console.log('Comments._collection.after.update')
  console.log(userId, doc, fieldNames, modifier, options)
  Items.update({_id: doc.referenceId}, {$set: {lastCommentedOn: new Date()}})
// sendCommentReplyEmails()
})
