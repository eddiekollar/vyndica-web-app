// Augment comments schema to include collection name of referenceId is for

Comments.addCustom = function (referenceId, collectionName, content) {
  Meteor.call('addCustomComment', referenceId, collectionName, content, function (error, result) {})
}

Comments.changeSchema(function (currentSchema) {
  currentSchema.collectionName = { type: String, optional: true }
})
