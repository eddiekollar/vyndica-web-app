NotificationSubscriptions = new Mongo.Collection('notficationsubscriptions');
Notifications = new Mongo.Collection('notifications');

notficationSubscriptionsSchema = new SimpleSchema({
    itemId : {
        type: String
    },
    subscribers: {
        type: [String]
    }
});

notificationsSchema = new SimpleSchema({
    itemId: {
        type: String
    },
    userId: {
        type: String
    },
    date: {
        type: Date,
        autoValue: function() {
            return new Date();
        }
    }
});

Notifications.helpers({
    getItem: function () {
        return Items.findOne({_id: this.itemId});
    }
});