Items = new Mongo.Collection('items')

var getStates = function () {
  return [
    {
      'label': 'Alabama',
      'value': 'AL'
    },
    {
      'label': 'Alaska',
      'value': 'AK'
    },
    {
      'label': 'Arizona',
      'value': 'AZ'
    },
    {
      'label': 'Arkansas',
      'value': 'AR'
    },
    {
      'label': 'California',
      'value': 'CA'
    },
    {
      'label': 'Colorado',
      'value': 'CO'
    },
    {
      'label': 'Connecticut',
      'value': 'CT'
    },
    {
      'label': 'Delaware',
      'value': 'DE'
    },
    {
      'label': 'District Of Columbia',
      'value': 'DC'
    },
    {
      'label': 'Florida',
      'value': 'FL'
    },
    {
      'label': 'Georgia',
      'value': 'GA'
    },
    {
      'label': 'Hawaii',
      'value': 'HI'
    },
    {
      'label': 'Idaho',
      'value': 'ID'
    },
    {
      'label': 'Illinois',
      'value': 'IL'
    },
    {
      'label': 'Indiana',
      'value': 'IN'
    },
    {
      'label': 'Iowa',
      'value': 'IA'
    },
    {
      'label': 'Kansas',
      'value': 'KS'
    },
    {
      'label': 'Kentucky',
      'value': 'KY'
    },
    {
      'label': 'Louisiana',
      'value': 'LA'
    },
    {
      'label': 'Maine',
      'value': 'ME'
    },
    {
      'label': 'Maryland',
      'value': 'MD'
    },
    {
      'label': 'Massachusetts',
      'value': 'MA'
    },
    {
      'label': 'Michigan',
      'value': 'MI'
    },
    {
      'label': 'Minnesota',
      'value': 'MN'
    },
    {
      'label': 'Mississippi',
      'value': 'MS'
    },
    {
      'label': 'Missouri',
      'value': 'MO'
    },
    {
      'label': 'Montana',
      'value': 'MT'
    },
    {
      'label': 'Nebraska',
      'value': 'NE'
    },
    {
      'label': 'Nevada',
      'value': 'NV'
    },
    {
      'label': 'New Hampshire',
      'value': 'NH'
    },
    {
      'label': 'New Jersey',
      'value': 'NJ'
    },
    {
      'label': 'New Mexico',
      'value': 'NM'
    },
    {
      'label': 'New York',
      'value': 'NY'
    },
    {
      'label': 'North Carolina',
      'value': 'NC'
    },
    {
      'label': 'North Dakota',
      'value': 'ND'
    },
    {
      'label': 'Ohio',
      'value': 'OH'
    },
    {
      'label': 'Oklahoma',
      'value': 'OK'
    },
    {
      'label': 'Oregon',
      'value': 'OR'
    },
    {
      'label': 'Pennsylvania',
      'value': 'PA'
    },
    {
      'label': 'Rhode Island',
      'value': 'RI'
    },
    {
      'label': 'South Carolina',
      'value': 'SC'
    },
    {
      'label': 'South Dakota',
      'value': 'SD'
    },
    {
      'label': 'Tennessee',
      'value': 'TN'
    },
    {
      'label': 'Texas',
      'value': 'TX'
    },
    {
      'label': 'Utah',
      'value': 'UT'
    },
    {
      'label': 'Vermont',
      'value': 'VT'
    },
    {
      'label': 'Virginia',
      'value': 'VA'
    },
    {
      'label': 'Washington',
      'value': 'WA'
    },
    {
      'label': 'West Virginia',
      'value': 'WV'
    },
    {
      'label': 'Wisconsin',
      'value': 'WI'
    },
    {
      'label': 'Wyoming',
      'value': 'WY'
    }
  ]
}

itemSchema = new SimpleSchema({
  title: {
    type: String
  },
  condition: {
    type: String,
    allowedValues: ['new', 'used'],
    autoform: {
      options: [
        { label: 'New', value: 'new' },
        { label: 'Used', value: 'used'}
      ]
    }
  },
  conditionType: {
    type: String,
    allowedValues: ['good', 'fair', 'poor'],
    optional: true,
    autoform: {
      value: 'good',
      options: [
        { label: 'Good', value: 'good'},
        { label: 'Fair', value: 'fair'},
        { label: 'Poor', value: 'poor'}
      ]
    }
  },
  status: {
    type: String,
    allowedValues: ['available', 'incycle' , 'unavailable', 'deleted'],
    autoValue: function () {
      if (this.isInsert) {
        return 'available'
      }
    },
    autoform: { omit: true }
  },
  description: {
    type: String,
    optional: true,
    max: 2000,
    autoform: {
      rows: 5
    }
  },
  tags: {
    type: [String],
    optional: true,
    autoform: {
      type: 'select',
      afFieldInput: {
        class: 'fluid search dropdown',
        multiple: true,
        allowAdditions: true,
        fullTextSearch: true
      },
      options: function () {
        var list = Interests.find().fetch()
        var options = []

        list.forEach(function (interest) {
          options.push({label: interest.title, value: interest._id})
        })
        return options
      }
    }
  },
  quantity: {
    type: Number,
    min: 1,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return 1
      }
    }
  },
  indegree: {
    type: Number,
    min: 0,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return 0
      }
    }
  },
  outdegree: {
    type: Number,
    min: 0,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return 0
      }
    }
  },
  img: {
    type: Object,
    blackbox: true,
    optional: true
  },
  'img._id': {
    type: String
  },
  'img.url': {
    type: String
  },
  city: {
    type: String,
    max: 50,
    optional: true
  },
  state: {
    type: String,
    optional: true,
    autoform: {
      options: function () {
        return getStates()
      }
    }
  },
  zip: {
    type: String,
    regEx: /^[0-9]{5}$/
  },
  easypostInfo: {
    type: Object,
    blackbox: true,
    optional: true
  },
  'easypostInfo.toAddressId': {
    type: String,
    optional: true
  },
  'easypostInfo.fromAddressId': {
    type: String,
    optional: true
  },
  ownerId: {
    type: String,
    denyUpdate: true,
    autoform: { omit: true },
    autoValue: function () {
      if (this.isInsert) {
        return this.userId
      } else if (this.isUpsert) {
        return {$setOnInsert: this.userId}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date()
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    },
    autoform: { omit: true }
  },
  updatedAt: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date()
      }
    },
    denyInsert: true,
    optional: true,
    autoform: { omit: true }
  },
  lastCommentedOn: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date()
      }
    },
    optional: true,
    autoform: { omit: true }
  }
})

Items.attachSchema(itemSchema)
Schemas.itemSchema = itemSchema

Items.helpers({
  imgURL: function (sizeParams) {
    var format = 'png'
    var url = ''
    if (this.img) {
      // format = this.img.res.format
      url = 'https://res.cloudinary.com/vyndica/image/upload/' + sizeParams + '/' + this.img._id + '.jpg'
    } else {
      url = 'https://placeholdit.imgix.net/~text?txtsize=35&txt=No+Image&w=250&h=250'
    }
    return url
  },
  ownerName: function () {
    var owner = Meteor.users.findOne({_id: this.ownerId})

    return owner.username
  },
  possibleTradesCount: function () {
    var regexp = new RegExp(this._id + '$', 'i')
    var incomingIds = _.pluck(Tradeedges.find({_id: {$regex: regexp}},
      {transform: function (doc) {
          // inItem is the item that makes up an incoming edge to the getItem
          doc.inItemId = doc._id.split(':')[0]
          return doc
        }
      }).fetch(), 'inItemId')
    return Items.find({ownerId: Meteor.userId(), _id: {$nin: incomingIds}}).count()
  }
})

if (Meteor.isClient) {
  var options = {
    keepHistory: 1000 * 60 * 5,
    localSearch: true
  }
  var fields = ['title', 'description']

  ItemsSearch = new SearchSource('itemsearch', fields, options)
}

ItemsIndex = new EasySearch.Index({
  collection: Items,
  fields: ['title', 'description'],
  engine: new EasySearch.MongoDB()
})

if (Meteor.isServer) {
  Items._ensureIndex({
    'title': 'text',
    'description': 'text'
  })
}

if (Meteor.Cordova) {
  Ground.Collection(Items)

  Ground.methodResume([
    'insertItem',
    'deleteItem',
    'updateItem'
  ])
}
