Items.after.insert(function(userId, doc) {
    NotificationSubscriptions.insert({itemId: doc._id, subscribers: [userId]});
});

Items.before.remove(function (userId, doc) {
    //outgoing edges from item
    var forwardPatt = new RegExp('^' + doc._id, 'i');
    Tradeedges.find({_id: {$regex: forwardPatt}}).forEach(function (edge) {
        Tradeedges.remove({_id: edge._id});
    });

    //incoming edges to item
    var backPatt = new RegExp(doc._id + '$', 'i');
    Tradeedges.find({_id: {$regex: backPatt}}).forEach(function (edge) {
        Tradeedges.remove({_id: edge._id});
    });
});