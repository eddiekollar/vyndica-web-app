Meteor.methods({
  deleteItem: function (docId) {
    check(docId, String)

    var item = Items.findOne({_id: docId})

    if (item && item.ownerId === Meteor.userId()) {
      Items.update({_id: docId}, {$set: {status: 'deleted'}})
    }
  },
  updateItem: function (docId, modifier) {
    check(docId, String)
    check(modifier, Object)

    var item = Items.findOne({_id: docId})

    if (item && item.ownerId === Meteor.userId()) {
      Items.update({_id: docId}, modifier)
    }
  },
  insertItem: function (doc) {
    check(doc, Object)
    console.log(JSON.stringify(doc))
    var id = Items.insert(doc)

    return id
  }
})
