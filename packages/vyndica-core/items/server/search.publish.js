var createTransform = function (userId, userLists) {
  return function (doc) {
    doc.isOwner = (doc.ownerId === userId)
    doc.isWanted = (userLists && userLists.wants && userLists.wants.indexOf(doc._id) > -1)

    return doc
  }
}

Meteor.publish('search', function (searchTerm) {
  check(searchTerm, Match.Any)

  var userLists = Lists.findOne({userId: this.userId})
  var transformFunc = createTransform(userLists)

  if (!searchTerm) {
    return Items.find({}, {transform: transformFunc})
  } else {
    return Items.find(
      {$text: {$search: searchTerm}},
      {
        fields: {
          score: {
            $meta: 'textScore'
          }
        },
        sort: {
          score: {
            $meta: 'textScore'
          }
        },
        transform: transformFunc
      }
    )
  }
})

var buildRegExp = function (searchText) {
  // this is a dumb implementation
  var parts = searchText.trim().split(/[ \-\:]+/)
  return new RegExp('(' + parts.join('|') + ')', 'ig')
}

SearchSource.defineSource('itemsearch', function (searchText, options) {
  // var options = {sort: {isoScore: -1}} // limit: 20

  var userLists = Lists.findOne({userId: this.userId})
  var transformFunc = createTransform(this.userId, userLists)

  if (searchText) {
    var regExp = buildRegExp(searchText)
    var selector = { $or: [
        {title: regExp},
        {description: regExp}
      ],
    status: {$nin: ['unavailable', 'deleted']}}

    return Items.find(selector, {transform: transformFunc}).fetch()
  } else {
    return Items.find({}, {transform: transformFunc}).fetch()
  }
})
