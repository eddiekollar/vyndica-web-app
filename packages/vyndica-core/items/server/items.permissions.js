Items.permit(['insert', 'update', 'remove']).ifLoggedIn().apply()
Items.permit(['update', 'remove']).isOwner().apply()
