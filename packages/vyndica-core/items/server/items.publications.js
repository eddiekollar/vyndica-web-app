const NUM_EDGES_OUT = 2

let createTransform = function (userId, userLists) {
  return function (doc) {
    doc.isOwner = (doc.ownerId === userId) ? true : false
    doc.isWanted = (userLists && userLists.wants && userLists.wants.indexOf(doc._id) > -1) ? true : false

    return doc
  }
}

Meteor.publishComposite('items', function (params) {
  check(params, Object)
  let userLists = Lists.findOne({userId: this.userId})

  if (params._id && params._id.$in) {
    if (userLists && userLists.wants) {
      params._id.$in = userLists.wants
    }
  }

  params['status'] = {$nin: ['unavailable', 'deleted']}

  return {
    find: function () {
      return Items.find(params, {transform: createTransform(this.userId, userLists), sort: {lastCommentedOn: -1, createdAt: -1}})
    },
    children: [
      {
        find: function (item) {
          return Meteor.users.find({_id: item.ownerId})
        }
      },
      {
        find: function (item) {
          return Interests.find()
        }
      },
      {
        find: function (item) {
          return Comments._collection.find({referenceId: item._id})
        }
      },
      {
        find: function (item) {
          const regexp = new RegExp(item._id + '$', 'i')
          return Tradeedges.find({_id: {$regex: regexp}, ownerId: this.userId})
        }
      }
    ]
  }
})

Meteor.publishComposite('graph', function (params) {
  check(params, Object)

  let selector = params.selector
  selector['status'] = {$nin: ['unavailable', 'deleted']}

  return {
    find: function () {
      return Items.find(selector, {sort: {lastCommentedOn: -1, createdAt: -1}})
    },
    children: [
      {
        find: function (item) {
          return Meteor.users.find({_id: item.ownerId})
        }
      },
      {
        find: function (item) {
          return Interests.find()
        }
      },
      {
        find: function (item) {
          return Comments._collection.find({referenceId: item._id})
        }
      },
      {
        find: function (item) {
          /* let outEdgesExp = (_id) => new RegExp('^' + _id, 'i')
          let inEdgesExp = (_id) => new RegExp(_id + '$', 'i') */

          let selectBuild = (ids) => {
            let select = {$or: []}
            _.each(ids, (_id) => {
              select.$or.push({_id: {$regex: new RegExp('^' + _id, 'i')}})
              select.$or.push({_id: {$regex: new RegExp(_id + '$', 'i')}})
            })
            return select
          }

          let selector = selectBuild([item._id])

          let tradeEdgeCursor = Tradeedges.find(selector)

          if (params.type === 'user' || params.type === 'item') {
            let allItemIds = [item._id]

            const numEdgesOut = (params.type === 'user') ? NUM_EDGES_OUT : NUM_EDGES_OUT - 1

            _.times(numEdgesOut, function (n) {
              let itemIds = []
              tradeEdgeCursor.forEach(function (tradeEdge) {
                ids = tradeEdge._id.split(':')
                itemIds = _.uniq(itemIds.concat(ids))
              })
              itemIds = _.filter(itemIds, (_id) => allItemIds.indexOf(_id) < 0)

              selector = selectBuild(itemIds)
              tradeEdgeCursor = Tradeedges.find(selector)
              allItemIds = allItemIds.concat(itemIds)
            }, this)

            selector = selectBuild(allItemIds)
            tradeEdgeCursor = Tradeedges.find(selector)
          }

          return tradeEdgeCursor
        },
        children: [
          {
            find: function (tradeEdge) {
              const ids = tradeEdge._id.split(':')

              return Items.find({_id: {$in: ids}, status: {$nin: ['unavailable', 'deleted']}})
            }
          }
        ]
      }
    ]
  }
})

Meteor.publishComposite('item', function (_id) {
  check(_id, String)
  const userLists = Lists.findOne({userId: this.userId})

  return {
    find: function () {
      return Items.find({$or: [{_id: _id}, {ownerId: this.userId}]}, {transform: createTransform(this.userId, userLists)})
    },
    children: [
      {
        find: function (item) {
          return Interests.find()
        }
      },
      {
        find: function (item) {
          return Meteor.users.find({_id: item.ownerId})
        }
      },
      {
        find: function (item) {
          return Comments._collection.find({referenceId: item._id})
        }
      },
      {
        find: function (item) {
          const regexp = new RegExp(item._id + '$', 'i')
          return Tradeedges.find({_id: {$regex: regexp}, ownerId: this.userId})
        }
      }
    ]
  }
})
