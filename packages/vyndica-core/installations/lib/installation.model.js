Installations = new Mongo.Collection('installations');

installationSchema = new SimpleSchema({
    channels: {
        type: [String],
        optional: true
    },
    agreedToTerms: {
        type: Boolean
    },
    deviceVersion: {
        type: String
    },
    deviceType: {
        type: String
    },
    deviceToken: {
        type: String
    },
    createdAt: {
        type: Date,
        denyUpdate: true,
        autoValue: function() {
          if (this.isInsert) {
            return new Date();
          } else if (this.isUpsert) {
            return {$setOnInsert: new Date()};
          } else {
            this.unset();  // Prevent user from supplying their own value
          }
        }
    },
    updatedAt: {
        type: Date,
        autoValue: function() {
            if (this.isUpdate) {
              return new Date();
            }
        },
        denyInsert: true,
        optional: true
    }
});

Installations.attachSchema(installationSchema);