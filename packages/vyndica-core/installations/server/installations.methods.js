Meteor.methods({
    insertInstallation: function(doc) {
        check(doc, Object);
        const installation = Installations.findOne({deviceToken: doc.deviceToken});
        
        if(!installation) {
            var id = Installations.insert(doc);
        }

        return true;
    },
    updateInstallation: function(deviceToken, modifier) {
        check(deviceToken, String);
        check(modifier, Object);
        Installations.update({deviceToken: doc.deviceToken}, modifier);
        return true;
    }
});