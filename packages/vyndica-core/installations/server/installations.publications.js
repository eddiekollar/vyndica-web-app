Meteor.publish('installation', function(deviceToken) {
    check(deviceToken, String);
    console.log('find installation with deviceToken: ', deviceToken);
    
    var installation = Installations.findOne({'deviceToken': deviceToken});
    console.log('Installation: ', JSON.stringify(installation));

    return Installations.find({'deviceToken': deviceToken});
});