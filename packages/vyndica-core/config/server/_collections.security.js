Security.defineMethod('isOwner', {
  fetch: ['ownerId'],
  deny: function (type, arg, userId,doc) {
    return userId !== doc.ownerId;
  }
});