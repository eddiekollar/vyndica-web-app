Meteor.methods({
  'createUserProfile': function (profile) {
    check(profile, Object)
    console.log('createUserProfile')
    console.log(profile)
    Match.test(profile, Schemas.profile)

    var id = Profiles.insert(profile)
    Meteor.users.update({_id: this.userId}, {$set: {'profile.setup': true}})

    return id
  },
  'hasSeenWelcome': function () {
    var ipaddress = this.connection.clientAddress
    var hasVisited = VisitorIPs.findOne({ipaddress: ipaddress})

    if (!hasVisited) {
      VisitorIPs.insert({ipaddress: ipaddress, dateVisited: new Date()})
    }

    return hasVisited
  },
  'updateUserProfile': function (modifier, docId) {
    check(modifier, Object)
    check(docId, String)

    console.log(modifier, docId)

    Profiles.update({_id: docId, userId: this.userId}, modifier)
  }
})
