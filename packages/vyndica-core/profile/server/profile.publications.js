Meteor.publishComposite('profile', function () {
  return {
    find: function () {
      return Profiles.find({userId: this.userId})
    }
  }
})

Profiles.permit(['insert', 'update']).ifLoggedIn().apply()
Profiles.permit(['update']).isOwner().apply()
