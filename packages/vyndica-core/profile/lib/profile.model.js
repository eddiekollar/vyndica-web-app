VisitorIPs = new Mongo.Collection('visitorips')
Profiles = new Mongo.Collection('profiles')
Addresses = new Mongo.Collection('addresses')

getStates = function () {
  return [
    {
      'label': 'Alabama',
      'value': 'AL'
    },
    {
      'label': 'Alaska',
      'value': 'AK'
    },
    {
      'label': 'Arizona',
      'value': 'AZ'
    },
    {
      'label': 'Arkansas',
      'value': 'AR'
    },
    {
      'label': 'California',
      'value': 'CA'
    },
    {
      'label': 'Colorado',
      'value': 'CO'
    },
    {
      'label': 'Connecticut',
      'value': 'CT'
    },
    {
      'label': 'Delaware',
      'value': 'DE'
    },
    {
      'label': 'District Of Columbia',
      'value': 'DC'
    },
    {
      'label': 'Florida',
      'value': 'FL'
    },
    {
      'label': 'Georgia',
      'value': 'GA'
    },
    {
      'label': 'Hawaii',
      'value': 'HI'
    },
    {
      'label': 'Idaho',
      'value': 'ID'
    },
    {
      'label': 'Illinois',
      'value': 'IL'
    },
    {
      'label': 'Indiana',
      'value': 'IN'
    },
    {
      'label': 'Iowa',
      'value': 'IA'
    },
    {
      'label': 'Kansas',
      'value': 'KS'
    },
    {
      'label': 'Kentucky',
      'value': 'KY'
    },
    {
      'label': 'Louisiana',
      'value': 'LA'
    },
    {
      'label': 'Maine',
      'value': 'ME'
    },
    {
      'label': 'Maryland',
      'value': 'MD'
    },
    {
      'label': 'Massachusetts',
      'value': 'MA'
    },
    {
      'label': 'Michigan',
      'value': 'MI'
    },
    {
      'label': 'Minnesota',
      'value': 'MN'
    },
    {
      'label': 'Mississippi',
      'value': 'MS'
    },
    {
      'label': 'Missouri',
      'value': 'MO'
    },
    {
      'label': 'Montana',
      'value': 'MT'
    },
    {
      'label': 'Nebraska',
      'value': 'NE'
    },
    {
      'label': 'Nevada',
      'value': 'NV'
    },
    {
      'label': 'New Hampshire',
      'value': 'NH'
    },
    {
      'label': 'New Jersey',
      'value': 'NJ'
    },
    {
      'label': 'New Mexico',
      'value': 'NM'
    },
    {
      'label': 'New York',
      'value': 'NY'
    },
    {
      'label': 'North Carolina',
      'value': 'NC'
    },
    {
      'label': 'North Dakota',
      'value': 'ND'
    },
    {
      'label': 'Ohio',
      'value': 'OH'
    },
    {
      'label': 'Oklahoma',
      'value': 'OK'
    },
    {
      'label': 'Oregon',
      'value': 'OR'
    },
    {
      'label': 'Pennsylvania',
      'value': 'PA'
    },
    {
      'label': 'Rhode Island',
      'value': 'RI'
    },
    {
      'label': 'South Carolina',
      'value': 'SC'
    },
    {
      'label': 'South Dakota',
      'value': 'SD'
    },
    {
      'label': 'Tennessee',
      'value': 'TN'
    },
    {
      'label': 'Texas',
      'value': 'TX'
    },
    {
      'label': 'Utah',
      'value': 'UT'
    },
    {
      'label': 'Vermont',
      'value': 'VT'
    },
    {
      'label': 'Virginia',
      'value': 'VA'
    },
    {
      'label': 'Washington',
      'value': 'WA'
    },
    {
      'label': 'West Virginia',
      'value': 'WV'
    },
    {
      'label': 'Wisconsin',
      'value': 'WI'
    },
    {
      'label': 'Wyoming',
      'value': 'WY'
    }
  ]
}

Schemas.address = new SimpleSchema({
  'userId': {
    type: String,
    denyUpdate: true,
    autoform: { omit: true },
    autoValue: function () {
      if (this.isInsert) {
        return this.userId
      } else if (this.isUpsert) {
        return {$setOnInsert: this.userId}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    }
  },
  'street1': {
    type: String,
    label: 'Street'
  },
  'street2': {
    type: String,
    optional: true,
    label: "Street con't"
  },
  'city': {
    type: String
  },
  'state': {
    type: String,
    autoform: {
      options: function () {
        return getStates()
      }
    }
  },
  'zip': {
    type: String
  }
})

Addresses.attachSchema(Schemas.address)

Schemas.profile = new SimpleSchema({
  'firstName': {
    type: String
  },
  'lastName': {
    type: String
  },
  'street1': {
    type: String,
    label: 'Street'
  },
  'street2': {
    type: String,
    optional: true,
    label: "Street Con't"
  },
  'city': {
    type: String
  },
  'state': {
    type: String,
    autoform: {
      type: 'select',
      options: function () {
        return getStates()
      }
    }
  },
  'zip': {
    type: String
  },
  'interests': {
    type: [String],
    optional: true
  },
  userId: {
    type: String,
    denyUpdate: true,
    autoform: { omit: true },
    autoValue: function () {
      if (this.isInsert) {
        return this.userId
      } else if (this.isUpsert) {
        return {$setOnInsert: this.userId}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    }
  },
  createdAt: {
    type: Date,
    denyUpdate: true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    },
    autoform: { omit: true }
  },
  updatedAt: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate) {
        return new Date()
      }
    },
    denyInsert: true,
    optional: true,
    autoform: { omit: true }
  }
})

Profiles.attachSchema(Schemas.profile)

Profiles.helpers({
  fullName: function () {
    var firstName = this.firstName || ''
    var lastName = this.lastName || ''

    return firstName + ' ' + lastName
  }
})
