Meteor.publishComposite('tradeedges', function () {
  return {
    find: function () {
      return Tradeedges.find({ownerId: this.userId})
    },
    children: [
      {
        find: function (tradeEdge) {
          const ids = tradeEdge._id.split(':')
          return Items.find({_id: {$in: ids}})
        }
      }, {
        find: function (tradeEdge) {
          return Tradecycles.find({tradeEdgeIds: {$in: [tradeEdge._id]}})
        }
      }
    ]
  }
})

Meteor.publishComposite('tradeloop', function (_id) {
  check(_id, String)

  return {
    find: function () {
      return Tradecycles.find({_id: _id})
    },
    children: [
      {
        find: function () {
          return Profiles.find({userId: this.userId})
        }
      },
      {
        find: function (tradecycle) {
          return Tradeedges.find({_id: {$in: tradecycle.tradeEdgeIds}})
        },
        children: [
          {
            find: function (tradedge) {
              var ids = tradedge._id.split(':')
              return Items.find({_id: {$in: ids}})
            }
          },
          {
            find: function (tradedge) {
              return Meteor.users.find({_id: tradedge.ownerId})
            }
          }
        ]
      },
      {
        find: function (tradecycle) {
          const itemIds = _.flatten(_.map(tradecycle.tradeEdgeIds, function (tradeEdgeId) {
            return tradeEdgeId.split(':')[0]}))
          return Items.find({_id: {$in: itemIds}})
        }
      }
    ]
  }
})

Meteor.publishComposite('tradeloops', function () {
  return {
    find: function () {
      return Tradecycles.find({userIds: {$in: [this.userId]}})
    },
    children: [

      {
        find: function (tradecycle) {
          const itemIds = _.flatten(_.map(tradecycle.tradeEdgeIds, function (tradeEdgeId) {
            return tradeEdgeId.split(':')[0]
          }))
          return Items.find({_id: {$in: itemIds}})
        }
      }
    ]
  }
})
