Meteor.methods({
  insertTradeEdge: function (upstreamItemId, tradeItemId) {
    check(upstreamItemId, String)
    check(tradeItemId, String)

    const tradeedgeId = upstreamItemId + ':' + tradeItemId
    if (!Tradeedges.findOne({_id: tradeedgeId})) {
      tx.start('create trade edge')
      Items.update({_id: upstreamItemId}, {$inc: {outdegree: 1}}, {tx: true})
      Items.update({_id: tradeItemId}, {$inc: {indegree: 1}}, {tx: true})

      const upstreamItem = Items.findOne({_id: upstreamItemId})
      const tradeItem = Items.findOne({_id: tradeItemId})

      Tradeedges.insert({_id: tradeedgeId}, {tx: true})
      /*
      Trigger push notifications
      */

      tx.commit()
      const swapEdge = Tradeedges.findOne({_id: tradeItemId + ':' + upstreamItemId})
      if (!swapEdge) {
        sendTradeOfferEmail(upstreamItem, tradeItem)
      }
      return true
    } else {
      return false
    }
  },
  deleteTradeedge: function (_id) {
    check(_id, String)
    const tradeedge = Tradeedges.findOne({_id: _id, ownerId: this.userId})

    if (tradeedge) {
      Tradeedges.remove({_id: _id})
    } else {
      console.log('No tradeedge found: ', _id, this.userId)
    }
  },
  buyShipping: function (cycleId) {
    check(cycleId, String)
    var cycle = Tradecycles.findOne({_id: cycleId})

    _.each(cycle.tradeedgeIds, function (edgeId) {
      var ids = edgeId.split(':')
      var upstreamItem = Items.findOne({_id: ids[0]})
      var tradeItem = Items.findOne({_id: ids[1]})
      var recepientId = upstreamItem.ownerId

      purchaseItemShipping(tradeItem, recepientId)
    })
  },
  createCycle: function (edgeIds) {
    check(edgeIds, Array)

    console.log({tradeEdgeIds: edgeIds})

    Tradecycles.insert({tradeEdgeIds: edgeIds})
  },
  confirmTradeEdge: function (info) {
    check(info, {
      addressId: String,
      nonce: String,
      tradeEdgeId: String
    })

    const ids = info.tradeEdgeId.split(':')

    Items.update({_id: ids[0]}, {$set: {'easypostInfo.fromAddressId': info.addressId}})
    Items.update({_id: ids[1]}, {$set: {'easypostInfo.toAddressId': info.addressId}})

    Tradeedges.update({_id: info.tradeEdgeId, ownerId: this.userId}, {$set: {status: 'confirmed'}})

    const tradecycle = Tradecycles.findOne({tradeEdgeIds: {$in: [info.tradeEdgeId]}})
    const tradeedgesCursor = Tradeedges.find({_id: {$in: tradecycle.tradeEdgeIds}})

    var allConfirmed = true
    var excludedEdgeIds = []

    tradeedgesCursor.forEach(function (edge) {
      allConfirmed = allConfirmed && edge.status === 'confirmed'
    })

    if (allConfirmed) {
      tradeedgesCursor.forEach(function (edge) {
        Tradeedges.find({_id: {$regex: edge._id.split(':')[0], $options: 'i'}, status: {$ne: 'confirmed'}}).forEach(function (tradeEdge) {
          excludedEdgeIds.push(tradeEdge._id)
        })
      })
      excludedEdgeIds = _.uniq(excludedEdgeIds)
      Tradecycles.update({_id: tradecycle._id }, {$set: {'status': 'confirmed', 'excludedEdgeIds': excludedEdgeIds}})
    }
  },
  cancelTradeEdge: function (edgeId, cycleId) {
    check(edgeId, String)
    check(cycleId, String)

    Tradeedges.remove({_id: edgeId, ownerId: this.userId})

    const tradecycle = Tradecycles.findOne({_id: cycleId})
    const tradeedgesCursor = Tradeedges.find({_id: {$in: tradecycle.tradeEdgeIds}})

    tradeedgesCursor.forEach(function (edge) {
      if (edge._id !== edgeId) {
        const ids = edge._id.split(':')

        // update all edges that were created while the cycle was waiting confirmation
        const backPatt = new RegExp(ids[0] + '$', 'i')
        Tradeedges.update({_id: {$regex: backPatt}, status: 'hold'}, {$set: {status: 'offered'}})

        // update incoming edges into edge._id.split(':')[0]
        Tradeedges.update({_id: edge._id}, {$set: {status: 'offered'}})
      }
    })

    Tradecycles.update({_id: cycleId}, {$set: {status: 'cancelled', cancelledEdgeId: edgeId}})
  }
})
