var forwardSearch = function (ids) {
  var forwardPatt = new RegExp('^' + ids[1], 'i')
  var forwardEdges = Tradeedges.find({_id: {$regex: forwardPatt}}).fetch()

  var cycles = []
  var cycleMap = {}
  cycleMap[ids[1]] = ids.join(':')

  // Create lists of IDs denoting the items in a cycle
  while(forwardEdges.length > 0) {
    var nextEdges = []
    var elem = cycleMap[edgeIds[0]]

    _.each(forwardEdges, function (edge) {
      var edgeIds = edge._id.split(':')
      var nextObj = cycleMap[edgeIds[1]]

      if (edgeIds[1] !== ids[0]) {
        // continue searching  
        var data
        if (elem[0].constructor === 'String') {
          data = elem.slice(0)
          data.push(edgeIds)

          if (nextObj) {
            if (nextObj[0].constructor === 'String') {
              elem = [data, nextObj]
            } else if (nextObj[0].constructor === 'Array') {
              nextObj.push(data)
              elem = nextObj
            }
          }
        } else if (elem[0].constructor === 'Array') {
          data = _.map(elem, function (arr) { return arr.push(edgeIds) })

          if (nextObj) {
            if (nextObj[0].constructor === 'String') {
              data.push(nextObj)
            } else if (nextObj[0].constructor === 'Array') {
              data.concat(nextObj)
            }
          }

          elem = data
        }

        cycleMap[edgeIds[1]] = elem

        forwardPatt = new RegExp('^' + edgeIds[1], 'i')
        nextEdges = _.union(nextEdges, Tradeedges.find({_id: {$regex: forwardPatt}}).fetch())
      } else {
        // closure found
        cycles.push(cycleMap[edgeIds[0]])
      }
    })

    delete cycleMap[edgeIds[0]]

    forwardEdges = nextEdges
  }
}

var tieBreaker = function (cycles) {
  /*
  Tie breaking for multiple closures:
  Average in degree per node/Average age on market + (1/size of graph)
  */
  var date = Date.now()
  var scores = _.map(cycles, function (cycle) {
    var ids = _.uniq(_.flatten(_.map(cycle, function (edge) { return edge.split(':')})))
    var ageSum = 0
    var indegreeSum = 0
    Items.find({_id: {$in: ids}}).forEach(function (item) {
      ageSum += item.createdAt - date
      indegreeSum += item.indegree
    })

    var ageAverage = ageSum / ids.length
    var indegreeAverage = indegreeSum / ids.length

    return (ageAverage / indegreeAverage) + 1 / ids.length
  })

  return cycles[scores.indexOf(_.max(scores))]
}

Tradeedges.after.insert(function (userId, doc) {
  const ids = doc._id.split(':')

  // Check that there are edges coming into the item in the zeroth position of the edge ID
  const upstreamItem = Items.findOne({_id: ids[0]})
  const tradeItem = Items.findOne({_id: ids[1]})

  if ((upstreamItem.status && upstreamItem.status === 'incycle') || (tradeItem.status && tradeItem.status === 'incycle')) {
    Tradeedges.update({_id: doc._id}, {$set: {status: 'hold'}})
    return
  } else {
    let cycles = []
    let tradeEdgeIds = []

    const backPatt = new RegExp(ids[0] + '$', 'i')
    const backEdgesCount = Tradeedges.find({_id: {$regex: backPatt}}).count()

    console.log('backEdgesCount: ', backEdgesCount)

    if (backEdgesCount === 0) {
      return
    }

    const swapEdgeId = ids.reverse().join(':')
    const swap = Tradeedges.findOne({_id: swapEdgeId})

    if (swap) {
      console.log('swap is happenging')
      cycles = [[doc._id, swapEdgeId]]
    }
    // cycles = forwardSearch(ids)

    if (cycles.length > 1) {
      console.log('tieBreaker: ', cycles)
      tradeEdgeIds = tieBreaker(cycles)
    } else if (cycles.length === 1) {
      tradeEdgeIds = cycles[0]
    }

    if (tradeEdgeIds.length > 0) {
      console.log('cycle happenging', tradeEdgeIds)

      Tradecycles.insert({tradeEdgeIds: tradeEdgeIds})
    }
  }
})

Tradeedges.after.remove(function (userId, doc) {
  // tx.start("delete trade edge")
  const ids = doc._id.split(':')
  Items.update({_id: ids[0]}, {$inc: {outdegree: -1}})
  Items.update({_id: ids[1]}, {$inc: {indegree: -1}})
// tx.commit()
})

Tradecycles.before.insert(function (userId, doc) {
  const itemIds = _.uniq(_.map(doc.tradeEdgeIds, function (edge) {
    return edge.split(':')[0]}))
  const userIds = _.uniq(_.pluck(Items.find({_id: {$in: itemIds}}, {fields: {ownerId: 1}}).fetch(), 'ownerId'))
  doc.userIds = userIds
})

Tradecycles.after.insert(function (userId, doc) {
  console.log('cycle created, ', JSON.stringify(doc))
  _.each(doc.tradeEdgeIds, function (edgeId) {
    Tradeedges.update({_id: edgeId}, {$set: {status: 'unconfirmed'}})
    sendTradeConfirmEmail(doc._id, edgeId)
  })

  const itemIds = _.uniq(_.map(doc.tradeEdgeIds, function (edge) { return edge.split(':')[0]}))
  Items.update({_id: {$in: itemIds}}, {$set: {status: 'incycle'}}, {multi: true})
})

/*
Tradecycles.before.update(function (userId, doc, fieldNames, modifier, options) {
  if (modifier.$set && modifier.$set['status'] === 'confirmed') {
    var itemIds = _.uniq(_.map(doc.tradeEdgeIds, function (edge) { return edge.split(':')[0]}))
    var excludedEdgeIds = []

    _.each(itemIds, function (itemId) {
      var excludedEdges = Tradeedges.find({_id: {$regex: itemId, $options: 'i'}, status: {$ne: 'confirmed'}}).fetch()
      excludedEdgeIds.push(_.pluck('_id', excludedEdges))
    })

    modifier.$set.excludedEdgeIds = excludedEdgeIds
  }
})
*/

Tradecycles.after.update(function (userId, doc, fieldNames, modifier, options) {
  if (fieldNames.indexOf('status') > -1) {
    const itemIds = _.uniq(_.map(doc.tradeEdgeIds, function (edge) { return edge.split(':')[0]}))

    if (modifier.$set['status'] === 'confirmed') {
      Items.update({_id: {$in: itemIds}}, {$set: {status: 'unavailable'}}, {multi: true})
      // purchase shipping labels

      _.each(doc.tradeEdgeIds, function (edgeId) {
        const itemId = edgeId.split(':')[0]
        const item = Items.findOne({_id: itemId})
        var shippingInfo = purchaseItemShipping(itemId)

        _.extend(shippingInfo, item.easypostInfo)
        Items.update({_id: itemId}, {$set: {easypostInfo: shippingInfo}})
        // Create label print page
        sendPrintShipLabelEmail(itemId)
      })

      Tradeedges.remove({_id: {$in: doc.tradeEdgeIds}})
      if (doc.excludedEdgeIds) {
        Tradeedges.remove({_id: {$in: doc.excludedEdgeIds}})
      }
    } else if (modifier.$set['status'] === 'cancelled') {
      // update all items to be available
      Tradeedges.update({_id: doc.tradeEdgeIds}, {$set: {status: 'offered', easypostAddressId: ''}}, {multi: true})
      Items.update({_id: {$in: itemIds}}, {$set: {status: 'available'}}, {multi: true})

      sendTradeLoopCancelEmail(doc._id)
    }
  }
})
