Tradeedges = new Mongo.Collection('tradeedges')
Tradecycles = new Mongo.Collection('tradecycles')

tradeedgeSchema = new SimpleSchema({
  _id: {
    type: String
  },
  status: {
    type: String,
    allowedValues: ['offered', 'unconfirmed', 'confirmed', 'hold', 'excluded', 'cancelled'],
    autoValue: function () {
      if (this.isInsert) {
        return 'offered'
      }
    }
  },
  ownerId: {
    type: String,
    denyUpdate: true,
    autoform: { omit: true },
    autoValue: function () {
      if (this.isInsert) {
        return this.userId
      } else if (this.isUpsert) {
        return {$setOnInsert: this.userId}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    }
  },
  createdAt: {
    type: Date,
    denyUpdate: true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    },
    autoform: { omit: true }
  }
})

tradecyclesSchema = new SimpleSchema({
  tradeEdgeIds: {
    type: [String]
  },
  excludedEdgeIds: {
    type: [String],
    autoValue: function () {
      if (this.isInsert) {
        return []
      }
    }
  },
  cancelledEdgeId: {
    type: String,
    autoValue: function () {
      if (this.isInsert) {
        return ''
      }
    }
  },
  userIds: {
    type: [String],
    autoValue: function () {
      if (this.isInsert) {
        return []
      }
    }
  },
  status: {
    type: String,
    allowedValues: ['pending', 'confirmed', 'cancelled', 'completed'],
    autoValue: function () {
      if (this.isInsert) {
        return 'pending'
      }
    }
  },
  createdAt: {
    type: Date,
    denyUpdate: true,
    autoValue: function () {
      if (this.isInsert) {
        return new Date
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()}
      } else {
        this.unset() // Prevent user from supplying their own value
      }
    },
    autoform: { omit: true }
  }
})

Tradeedges.attachSchema(tradeedgeSchema)
Tradecycles.attachSchema(tradecyclesSchema)
