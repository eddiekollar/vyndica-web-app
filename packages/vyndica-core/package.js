Package.describe({
  name: 'vyndica:core',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
})

Package.onUse(function (api) {
  api.versionsFrom('1.2.1')

  api.use(['mongo',
    'ejson',
    'email',
    'underscore',
    'ecmascript',
    'ongoworks:security',
    'reywood:publish-composite',
    'matb33:collection-hooks',
    'aldeed:autoform',
    'easy:search',
    'meteorhacks:search-source',
    'meteorhacks:fast-render',
    'meteorhacks:subs-manager',
    'arkham:comments-ui',
    'aldeed:collection2',
    'iron:router',
    'babrahams:transactions',
    'ground:db',
    'lepozepo:cloudinary',
    'percolate:synced-cron',
    'wylio:mandrill'])

  api.addFiles(['config/lib/schemas.js',
    'interests/lib/interests.model.js',
    'installations/lib/installation.model.js',
    'items/lib/item.model.js',
    'notifications/lib/notifications.model.js',
    'notifications/lib/comments.model.js',
    'lists/lib/lists.model.js',
    'profile/lib/profile.model.js',
    'trade-edges/lib/trades.model.js'], ['client', 'server'])

  // api.addFiles(['items/lib/items.routes.js'], ['web.browser', 'server'])

  api.addFiles(['config/server/config.js',
    'config/server/_collections.security.js',
    'interests/server/interests.permissions.js',
    'items/server/items.hooks.js',
    'items/server/items.methods.js',
    'items/server/items.permissions.js',
    'items/server/items.publications.js',
    'items/server/search.publish.js',
    'installations/server/installations.methods.js',
    'installations/server/installations.publications.js',
    'notifications/server/comments.hooks.js',
    'notifications/server/comments.methods.js',
    'notifications/server/notifications.methods.js',
    'notifications/server/notifications.publications.js',
    'lists/server/lists.methods.js',
    'lists/server/lists.publications.js',
    'profile/server/profile.methods.js',
    'profile/server/profile.publications.js',
    'trade-edges/server/trades.hooks.js',
    'trade-edges/server/trades.publications.js',
    'trade-edges/server/trades.methods.js',
    'automation/shipping.js',
    'automation/emails.js',
    'automation/payments.js'], 'server')

  api.export(['Schemas', 'Interests', 'Items', 'ItemsIndex', 'Comments', 'Notifications', 'NotificationSubscriptions', 'Lists',
    'Profiles',
    'VisitorIPs',
    'ItemsSearch',
    'Installations',
    'Tradeedges',
    'Tradecycles',
    'SyncedCron'])
})

Npm.depends({
  'node-easypost': '2.0.10',
  'braintree': '1.38.0'
})
