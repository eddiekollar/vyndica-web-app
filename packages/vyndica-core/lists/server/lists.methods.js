Meteor.methods({
    updateList: function(listName, modifier) {
        check(listName, String);
        check(modifier, Object);

        var userLists = Lists.findOne({userId: this.userId});

        if(!userLists) {
            Lists.insert({userId: this.userId});
        }

        Lists.update({userId: this.userId}, modifier);

        var op = _.keys(modifier)[0];
        var itemId = _.values(modifier[op])[0];

        mod = {};
        mod[op] = {subscribers: itemId};

        console.log('Update subscribers: ', JSON.stringify(mod));
        NotificationSubscriptions.update({itemId: itemId}, mod);

        return true;
    }

});