Easypost = Npm.require('node-easypost')
var easypost = Easypost(Meteor.settings.EASYPOST.API_KEY)

var parcel = {
  // height: 0,
  // width: 0,
  // length: 0,
  weight: 1
}

getAddress = function (userId) {
  const profile = Profile.findOne({userId: userId})
  var address = {}

  if (profile) {
    address = {
      name: profile.fullName(),
      street1: profile.street1,
      street2: profile.street2 || '',
      city: profile.city,
      state: profile.state,
      zip: profile.zip
    }
  }
  return address
}

purchaseItemShipping = function (itemId) {
  const item = Items.findOne({_id: itemId})

  const toAddress = {
    id: item.easypostInfo.toAddressId
  }
  const fromAddress = {
    id: item.easypostInfo.fromAddressId
  }

  const shipment = {
    to_address: toAddress,
    from_address: fromAddress,
    parcel: parcel
  }

  Future = Npm.require('fibers/future')
  var fut = new Future()

  easypost.Shipment.create(
    shipment,
    function (err, shipment) {
      if (err) {
        console.error('Error in retrieve of purchaseShipping', JSON.stringify(err))
        fut.throw(err)
      } else {
        shipment.buy({rate: shipment.lowestRate(['USPS', 'ParcelSelect'])}, function (err, shipment) {
          if (err) {
            console.error('Error shipment buy error: ', JSON.stringify(err))
            fut.throw(err)
          } else {
            console.log(JSON.stringify(shipment))
            console.log(shipment.tracking_code)
            console.log(shipment.postage_label.label_url)
            const url = (shipment.postage_label && shipment.postage_label.label_url) ? shipment.postage_label.label_url : ''
            fut.return({
              shipmentId: shipment.id,
              trackingCode: shipment.tracking_code,
              labelURL: url
            })
          }
        })
      }
    })

  try {
    return fut.wait()
  } catch (err) {
    throw new Meteor.Error('futures error', err)
  }
}

Meteor.methods({
  /*
  create new parcel when item is created?
  */
  createParcel: function () {
    easypost.Parcel.create(
      parcel
      , function (err, response) {
        console.error('Easypost parcel error: ', JSON.stringify(err))
        console.log('Easypost parcel response: ', response)
      })
  },
  /*
      * create shipments when edges are created for estmates (soon)
      * create shipments when trade is confirmed
  should be able to display estimated shipping cost in the future
  */
  createShipment: function () { /*
    var shipment = {
      to_address: toAddress,
      from_address: fromAddress,
      parcel: parcel
    }

    console.log('createShipment')
    easypost.Shipment.create(shipment,
      function (err, shipment) {
        if (err) {
          console.error('Easypost shipment error: ', JSON.stringify(err))
        } else {
          // Save id
        }
      })*/
  },
  refundShipping: function (shipmentId) {
    check(shipmentId, String)

    easypost.Shipment.retrieve({id: shipmentId}, function (err, shipment) {
      if (err) {
        console.error(err)
      } else {
        shipment.refund({}, function (err, shipment) {
          if (err) {
            console.error('on refund: ', err)
          } else {
            console.log('refund:', JSON.stringify(shipment))
          }
        })
      }
    })
  },
  estimateShipping: function (toAddress, fromAddress) {
    check(toAddress, Object)
    check(fromAddress, Object)

    const shipment = {
      to_address: toAddress,
      from_address: fromAddress,
      parcel: parcel
    }

    console.log(JSON.stringify(shipment))

    easypost.Shipment.create(
      shipment,
      function (err, shipment) {
        if (err) {
          console.error('Error in retrieve of purchaseShipping', JSON.stringify(err))
        } else {
          // Purchase
          console.log(JSON.stringify(shipment))
        }
      }
    )
  },
  purchaseShipping: function (toAddress, fromAddress, parcel) {
    check(toAddress, Object)
    check(fromAddress, Object)

    const shipment = {
      to_address: toAddress,
      from_address: fromAddress,
      parcel: parcel
    }

    console.log(JSON.stringify(shipment))

    easypost.Shipment.create(
      shipment,
      function (err, shipment) {
        if (err) {
          console.error('Error in retrieve of purchaseShipping', JSON.stringify(err))
        } else {
          // Purchase
          // console.log(JSON.stringify(shipment))

          shipment.buy({rate: shipment.lowestRate(['USPS'])}, function (err, shipment) {
            if (err) {
              console.error('Error shipment buy error: ', JSON.stringify(err))
            } else {
              console.log(JSON.stringify(shipment))
              console.log(shipment.tracking_code)
              console.log(shipment.postage_label.label_url)
            }
          })
        }
      })
  },
  verifyAddress: function (userInfo) {
    check(userInfo, Object)

    const info = {
      verify_strict: ['delivery'],
      address: userInfo
    }

    const userId = this.userId
    console.log(JSON.stringify(info))

    Future = Npm.require('fibers/future')
    const fut = new Future()

    easypost.Address.create(
      info,
      Meteor.bindEnvironment(function (err, address) {
        if (err) {
          fut.throw(err)
        } else {
          const profile = Profiles.findOne({userId: userId})
          const verified = (address.verifications && address.verifications.delivery && address.verifications.delivery.success)
          if (!profile && verified) {
            delete userInfo.name
            userInfo.userId = userId
            userInfo.interests = []
            Profiles.insert(userInfo)
          }
          fut.return((verified) ? {addressId: address.id} : false)
        }
      })
    )
    try {
      return fut.wait()
    } catch (err) {
      throw new Meteor.Error('futures error', err)
    }
  }

})
/*

*/
