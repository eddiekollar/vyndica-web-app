const oneday = 24 * 60 * 60 * 1000

sendEmail = function (subject, html) {
  var emails = ['eddie@vyndica.com']

  if (Meteor.settings.public.environment !== 'development') {
    emails.push('nevanich@vyndica.com')
  }

  Email.send({
    to: emails,
    from: 'hello@vyndica.com',
    subject: subject,
    html: html
  })
}

sendMandrillEmail = function (template_name, subject, recipients, global_merge_vars, merge_vars) {
  console.log('sending email: ', JSON.stringify(recipients))
  Mandrill.messages.sendTemplate({
    template_name: template_name,
    template_content: [],
    message: {
      subject: subject,
      from_email: 'hello@vyndica.com',
      from_name: 'Vyndica',
      to: recipients,
      // global merge variable in the *|VARIABLE|* format
      global_merge_vars: global_merge_vars,
      // per-recipient merge vars
      merge_vars: merge_vars
    }
  })
}

sendCommentEmails = function (doc) {
  if (doc.collectionName === 'items') {
    sendItemCommentEmails(doc)
  } else if (doc.collectionName === 'tradecycles') {
    sendTradeCommentEmails(doc)
  }
}

sendItemCommentEmails = function (doc) {
  const item = Items.findOne({_id: doc.referenceId})

  if (item) {
    const owner = Meteor.users.findOne({_id: item.ownerId})
    const profile = Profiles.findOne({userId: item.ownerId})

    const commenter = Meteor.users.findOne({_id: doc.userId})
    const comment = doc.content

    var template_name, subject = ''
    const global_merge_vars = [
      {
        name: 'UI_LINK',
        content: Meteor.absoluteUrl() + 'items/' + item._id
      },
      {
        name: 'UI_NM',
        content: item.title
      },
      {
        name: 'UI_IMAGE',
        content: item.imgURL('h_250,w_250,c_fit,q_50')
      },
      {
        name: 'UI_CMNT_LN',
        content: Meteor.absoluteUrl() + 'items/' + item._id
      },
      {
        name: 'UI_CMNT',
        content: comment
      }]
    var merge_vars, recipients = []

    // send email to owner
    template_name = 'new-comment'
    subject = 'Someone commmented on your item!'
    recipients.push({email: owner.emails[0].address})
    let fname = (profile && profile.firstName) ? profile.firstName : owner.username
    let lname = (profile && profile.lastName) ? profile.lastName : ''
    merge_vars = [{
      rcpt: owner.emails[0].address,
      vars: [
        {
          name: 'fname',
          content: fname
        },
        {
          name: 'lname',
          content: lname
        }
      ]
    }]

    sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)

    // send email to all of those are trading for this item
    template_name = 'new-comment-on-want-item'
    subject = 'Someone commmented on an item you want!'
    merge_vars, recipients = []

    // find all incoming edges
    const pattern = new RegExp(item._id + '$', 'i')
    const incomingIds = _.pluck(Tradeedges.find({_id: {$regex: pattern}},
      { transform: function (doc) {
          // inItem is the item that makes up an incoming edge to the getItem
          doc.inItemId = doc._id.split(':')[0]
          return doc
        }
      }).fetch(), 'inItemId')

    const userIds = _.uniq(_.pluck(Items.find({_id: {$in: incomingIds}}).fetch(), 'ownerId'))

    _.each(userIds, function (id) {
      if (id !== doc.userId) {
        const user = Meteor.users.findOne({_id: id})
        const profile = Profiles.findOne({userId: id})

        recipients.push({email: user.emails[0].address})

        fname = (profile && profile.firstName) ? profile.firstName : owner.username
        lname = (profile && profile.lastName) ? profile.lastName : ''
        merge_vars.push({
          rcpt: user.emails[0].address,
          vars: [
            {
              name: 'fname',
              content: fname
            },
            {
              name: 'lname',
              content: lname
            }
          ]
        })
      }
    })

    sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)
  }
}

sendTradeCommentEmails = function (doc) {
  const tradeloop = Tradecycles.findOne({_id: doc.referenceId})

  if (tradeloop) {
    const comment = doc.content

    const template_name = 'new-trade-loop-comment'
    const subject = 'Someone commented on a trade'
    const global_merge_vars = []

    _.each(tradeloop.tradeEdgeIds, function (edgeId) {
      const ids = edgeId.split(':')

      const upstreamItem = Items.findOne({_id: ids[0]})

      // Don't email the commenter
      if (upstreamItem.ownerId === doc.userId) {
        return
      }

      const tradeItem = Items.findOne({_id: ids[1]})
      const recipient = Meteor.users.findOne({_id: upstreamItem.ownerId})
      const profile = Profiles.findOne({userId: upstreamItem.ownerId})

      const tradeActionLink = Meteor.absoluteUrl() + 'tradeloops/' + tradeloop._id

      const fname = (profile && profile.firstName) ? profile.firstName : recipient.username
      const lname = (profile && profile.lastName) ? profile.lastName : ''

      const recipients = [{email: recipient.emails[0].address}]

      const merge_vars = [
        {
          rcpt: recipient.emails[0].address,
          vars: [
            {
              name: 'fname',
              content: fname
            },
            {
              name: 'lname',
              content: lname
            },
            {
              name: 'UI_LINK',
              content: tradeActionLink
            },
            {
              name: 'UI_IMAGE',
              content: upstreamItem.imgURL('h_250,w_250,c_fit,q_50')
            },
            {
              name: 'UI_NM',
              content: upstreamItem.title
            },
            {
              name: 'TL_URL',
              content: tradeActionLink
            },
            {
              name: 'TI_IMAGE',
              content: tradeItem.imgURL('h_250,w_250,c_fit,q_50')
            },
            {
              name: 'TI_NM',
              content: tradeItem.title
            },
            {
              name: 'TL_CMNT',
              content: comment
            }
          ]
        }
      ]

      sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)
    })
  }
}

sendTradeConfirmEmail = function (tradecycleId, edgeId) {
  const ids = edgeId.split(':')

  const upstreamItem = Items.findOne({_id: ids[0]})
  const tradeItem = Items.findOne({_id: ids[1]})

  const template_name = 'trade-confirmation'
  const subject = 'You have a trade to confirm'
  const userId = upstreamItem.ownerId
  const recipient = Meteor.users.findOne({_id: userId})
  const profile = Profiles.findOne({userId: recipient._id})
  const recipients = [{email: recipient.emails[0].address}]
  const global_merge_vars = []

  const tradeCycle = Tradecycles.findOne({_id: tradecycleId})
  const tradeActionLink = Meteor.absoluteUrl() + 'tradeloops/' + tradecycleId

  const fname = (profile && profile.firstName) ? profile.firstName : recipient.username
  const lname = (profile && profile.lastName) ? profile.lastName : ''

  const merge_vars = [
    {
      rcpt: recipient.emails[0].address,
      vars: [
        {
          name: 'fname',
          content: fname
        },
        {
          name: 'lname',
          content: lname
        },
        {
          name: 'UI_LINK',
          content: tradeActionLink
        },
        {
          name: 'UI_IMAGE',
          content: upstreamItem.imgURL('h_250,w_250,c_fit,q_50')
        },
        {
          name: 'UI_NM',
          content: upstreamItem.title
        },
        {
          name: 'TL_URL',
          content: tradeActionLink
        },
        {
          name: 'TI_IMAGE',
          content: tradeItem.imgURL('h_250,w_250,c_fit,q_50')
        },
        {
          name: 'TI_NM',
          content: tradeItem.title
        },
        {
          name: 'TRADE_CONF',
          content: tradeActionLink
        },
        {
          name: 'TRADE_CANX',
          content: tradeActionLink
        },
        {
          name: 'TRADE_NUM',
          content: tradeCycle.tradeEdgeIds.length
        }
      ]
    }
  ]

  sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)
}

sendTradeOfferEmail = function (upstreamItem, tradeItem) {
  var subject = 'Someone is interested in one of your items'
  var template_name = 'someone-wants-your-thing'

  var userId = tradeItem.ownerId
  var recipient = Meteor.users.findOne({_id: userId})
  var profile = Profiles.findOne({userId: recipient._id})
  var to = [{email: recipient.emails[0].address}]
  var global_merge_vars = []

  var fname = (profile && profile.firstName) ? profile.firstName : recipient.username
  var lname = (profile && profile.lastName) ? profile.lastName : ''

  var merge_vars = [
    {
      rcpt: recipient.emails[0].address,
      vars: [
        {
          name: 'fname',
          content: fname
        },
        {
          name: 'lname',
          content: lname
        },
        {
          name: 'UI_LINK',
          content: Meteor.absoluteUrl() + 'items/' + tradeItem._id
        },
        {
          name: 'UI_IMAGE',
          content: tradeItem.imgURL('h_250,w_250,c_fit,q_50')
        },
        {
          name: 'UI_ITEM_DS',
          content: tradeItem.title
        },
        {
          name: 'TI_LINK',
          content: Meteor.absoluteUrl() + 'items/' + upstreamItem._id
        },
        {
          name: 'TI_IMAGE',
          content: upstreamItem.imgURL('h_250,w_250,c_fit,q_50')
        },
        {
          name: 'TI_ITEM_DS',
          content: upstreamItem.title
        },
        {
          name: 'TI_GET',
          content: Meteor.absoluteUrl() + 'items/' + upstreamItem._id
        }
      ]
    }
  ]

  sendMandrillEmail(template_name, subject, to, global_merge_vars, merge_vars)
}

sendTradeLoopCancelEmail = function (tradecycleId) {
  const cycle = Tradecycles.findOne({_id: tradecycleId})
  const subject = 'Someone in the trade loop cancelled'
  const template_name = 'someone-cancelled'

  let recipients = []
  // all trade info
  const global_merge_vars = [
    {
      name: 'TL_URL',
      content: Meteor.absoluteUrl() + 'tradeloops/' + tradecycleId
    }
  ]
  // per user data
  let merge_vars = []

  let itemIds = _.uniq(_.flatten(_.map(cycle.tradeEdgeIds, function (edge) {
    return edge.split(':')
  })))

  itemIds = _.filter(itemIds, (id) => {
    return id !== cycle.cancelledEdgeId.split(':')[0]})

  _.each(itemIds, function (itemId) {
    const item = Items.findOne({_id: itemId})
    /*
    Format html based on items
    Show status of each edge
    */

    const recipient = Meteor.users.findOne({_id: item.ownerId})
    const profile = Profiles.findOne({userId: item.ownerId})

    const fname = (profile && profile.firstName) ? profile.firstName : recipient.username
    const lname = (profile && profile.lastName) ? profile.lastName : ''

    const mergeVarObj = {
      rcpt: recipient.emails[0].address,
      vars: [
        {
          name: 'fname',
          content: fname
        },
        {
          name: 'lname',
          content: lname
        },
        {
          name: 'UI_IMAGE',
          content: item.imgURL('h_250,w_250,c_fit,q_50')
        },
        {
          name: 'UI_NM',
          content: item.title
        },
        {}
      ]
    }

    recipients.push({email: recipient.emails[0].address})
    merge_vars.push(mergeVarObj)
  })

  sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)
}

sendPrintShipLabelEmail = function (itemId) {
  const item = Items.findOne({_id: itemId})
  const template_name = 'trade-loop-confirmed-shipping-label'
  const subject = 'Trade Loop Confirmed & Shipping Label'
  const recipient = Meteor.users.findOne({_id: item.ownerId})
  const profile = Profiles.findOne({userId: recipient._id})
  const recipients = [{email: recipient.emails[0].address}]
  const global_merge_vars = []

  const fname = (profile && profile.firstName) ? profile.firstName : recipient.username
  const lname = (profile && profile.lastName) ? profile.lastName : ''

  const merge_vars = [
    {
      rcpt: recipient.emails[0].address,
      vars: [
        {
          name: 'fname',
          content: fname
        },
        {
          name: 'lname',
          content: lname
        },
        {
          name: 'UI_LABEL',
          content: Meteor.absoluteUrl() + 'items/' + item._id + '/shippingprint'
        },
        {
          name: 'UI_LINK',
          content: Meteor.absoluteUrl() + 'items/' + item._id
        },
        {
          name: 'UI_NM',
          content: item.title
        },
        {
          name: 'UI_IMAGE',
          content: item.imgURL('h_250,w_250,c_fit,q_50')
        }
      ]
    }
  ]

  sendMandrillEmail(template_name, subject, recipients, global_merge_vars, merge_vars)
}

function lastDaysTradeedges () {
  console.log('lastDaysTradeedges')
  let html = ''

  const date = new Date(Date.now() - oneday)
  const selector = {createdAt: {$gt: date}}

  const tradeedgeCursor = Tradeedges.find(selector)

  const subject = '[Trade Proposal Report][' + Meteor.settings.public.environment + ']' + tradeedgeCursor.count() + ' new trade proposals'
  if (tradeedgeCursor.count() === 0) {
    html = 'Nothing to see here...'
  } else {
    tradeedgeCursor.forEach(function (edge) {
      const ids = edge._id.split(':')
      const items = Items.find({_id: {$in: ids}}).fetch()

      html += 'Trade edge: <br>'
      html += items[0].title + '(id: ' + items[0]._id + ') for ' + items[1].title + '(id: ' + items[1]._id + ') <br> <br>'
    })
  }

  sendEmail(subject, html)
}

SyncedCron.add({
  name: 'Trade Edge Report',
  schedule: function (parser) {
    // parser is a later.parse object
    return parser.recur().on(0).hour()
  },
  job: function () {
    lastDaysTradeedges()
  }
})
