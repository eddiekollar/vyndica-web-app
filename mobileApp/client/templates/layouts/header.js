Template.header.onRendered(function() {
    console.log(Session.get('headerTitle'));
});

Template.header.helpers({
    canGoBack: function () {
        var goBack = (Session.get('showBack') === undefined) ? false : Session.get('showBack');
        return goBack;
    },
    getHeaderTitle: function() {
        return Session.get('headerTitle');
    }
});

Template.header.events({
    'click [data-action=goback]': function () {
        history.back();
    }
});