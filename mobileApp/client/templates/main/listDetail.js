Template.listDetail.helpers({
    getCommentCount: function() {
        return Comments._collection.find({referenceId: this._id}).count();
    }
});

Template.listDetail.events({
    'click .clickable': function (event, template) {
        Router.go('item.show', {_id: template.data._id});
    }
});