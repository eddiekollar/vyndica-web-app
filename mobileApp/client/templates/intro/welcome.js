Template.welcome.onRendered(function() {
    //timer move to next page if no tos agreement or agreed
    Meteor.setTimeout(function() {
        Router.go('accept.terms', {}, {replaceState: true});
    }, 3000);
});