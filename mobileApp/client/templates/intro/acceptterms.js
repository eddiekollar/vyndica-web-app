Template.acceptterms.helpers({
    isBeta: function () {
        return true;
    }
});

Template.acceptterms.onCreated(function() {
    var uuid = (deviceInfo) ? deviceInfo.uuid : '';
    var installation = Installations.findOne({'deviceToken': uuid});
    console.log(installation);
    if (Meteor.status().connected) {
        alert('connected');
    }else{
        alert('disconnected');
    }

    if(installation && installation.agreedToTerms) {
        Router.go('items', {}, {replaceState: true});
    }else{
        if(deviceInfo) {
            installation = {
                channels: [],
                deviceToken: deviceInfo.uuid,
                deviceType: deviceInfo.platform.toLowerCase(),
                deviceVersion: deviceInfo.version
            }
        }
    }
    this.installation = (installation) ? installation : {};   
});

Template.acceptterms.events({
    'click [data-action=accept]': function (event, template) {
        /*
        if(Meteor.isCordova) {
            var installation = template.installation;
            installation.agreedToTerms = true;
            Meteor.call('insertInstallation', installation, function(err, result) {
                if(err) {
                    alert(err);
                }else{
                    alert('success');
                    Router.go('items', {}, {replaceState: true});
                }
            });
        }else{
            Router.go('items', {}, {replaceState: true});
        }
        */
        Router.go('items', {}, {replaceState: true});
    },
    'click [data-action=decline]': function () {
        // Meteor.call('acceptTerms', device, false, ... )
    }
});