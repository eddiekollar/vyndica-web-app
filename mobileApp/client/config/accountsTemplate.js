var pwd = AccountsTemplates.removeField('password');
AccountsTemplates.removeField('email');

AccountsTemplates.addField({
    _id: 'username',
    type: 'text',
    required: true,
    func: function(value){
        if (Meteor.isClient) {
            console.log("Validating username...");
            var self = this;
            Meteor.call("userExists", value, function(err, userExists){
                if (!userExists)
                    self.setSuccess();
                else
                    self.setError(userExists);
                self.setValidating(false);
            });
            return;
        }
        // Server
        return Meteor.call("userExists", value);
    }
});

AccountsTemplates.addField({
    _id: 'email',
    type: 'email',
    required: true,
    displayName: "email",
    re: /.+@(.+){2,}\.(.+){2,}/,
    errStr: 'Invalid email',
    func: function(value){
        if (Meteor.isClient) {
            console.log("Validating email...");
            var self = this;
            Meteor.call("emailExists", value, function(err, userExists){
                if (!userExists)
                    self.setSuccess();
                else
                    self.setError(userExists);
                self.setValidating(false);
            });
            return;
        }
        // Server
        return Meteor.call("emailExists", value);
    }
});
AccountsTemplates.addField(pwd);

var onSubmitHook = function(error, state) {
    if (!error) {
        if (state === "signIn" || state === "signUp") {
            //var route = Session.get('intendedRoute');
            Router.go('items', {}, {replaceState: true});
        }
    }
};

AccountsTemplates.configure({
    homeRoutePath: '/',
    onSubmitHook: onSubmitHook,
    showForgotPasswordLink: true,
    texts: {
      title: {
        signUp: ' ',
        signIn: ' '
      }
    }
});

//AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');