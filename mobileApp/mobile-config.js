App.info({
  id: 'com.vyndica.mobile',
  name: 'Vyndica',
  description: 'A group bartering site that algorithmically includes as many people needed to satisfy all trades.',
  author: 'Vyndica',
  email: 'hello@vyndica.com',
  website: 'http://www.vyndica.com'
});

App.accessRule('*');
App.accessRule('blob:*');

App.icons({
  'android_ldpi': 'resources/icons/android/drawable-ldpi/ic_launcher.png',
  'android_mdpi': 'resources/icons/android/drawable-mdpi/ic_launcher.png',
  'android_hdpi': 'resources/icons/android/drawable-hdpi/ic_launcher.png',
  'android_xhdpi': 'resources/icons/android/drawable-xhdpi/ic_launcher.png'
});

App.launchScreens({
  'android_ldpi_portrait': 'resources/screens/android/screen-ldpi-portrait.png',
  'android_mdpi_portrait': 'resources/screens/android/screen-mdpi-portrait.png',
  'android_hdpi_portrait': 'resources/screens/android/screen-hdpi-portrait.png',
  'android_xhdpi_portrait': 'resources/screens/android/screen-xhdpi-portrait.png'
});

/*
  // iOS
  'iphone': 'resources/icons/icon-60x60.png',
  'iphone_2x': 'resources/icons/icon-60x60@2x.png',
  'iphone_3x': 'resources/icons/icon-60x60@3x.png',
  'ipad': 'resources/icons/icon-76x76.png',
  'ipad_2x': 'resources/icons/icon-76x76@2x.png',

*/