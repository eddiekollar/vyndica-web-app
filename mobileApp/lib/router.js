IntroController = RouteController.extend({
    layoutTemplate: 'basic'
});

AppController = RouteController.extend({
  layoutTemplate: 'appLayout',
  yieldTemplates: {
    'header': {to: 'header'},
    'footer': {to: 'footer'}
  }
});

Router.map(function () {
    this.route('welcome', {
        path: '/',
        onBeforeAction: function() {
            //localstorage check if visited and accepted tos
            this.next();
        },
        controller: 'IntroController',
        template: 'welcome'
    });

    this.route('accept.terms', {
        path: '/acceptterms',
        template: 'acceptterms',
        controller: 'IntroController'
        
    });

    /*
,
        waitOn: function () {
            var uuid = (Meteor.isCordova) ? (deviceInfo.uuid) : '';
            return Meteor.subscribe('installation', uuid);
        }
    */

    this.route('full.terms', {
        path: '/termsofuse',
        controller: 'IntroController',
        template: 'termsofuse'
    });

    this.route('new.features', {
        path: '/newfeatures',
        controller: 'IntroController',
        template: 'newfeatures'
    });

    this.route('login', {
        path: '/login',
        controller: 'AppController',
        template: 'login'
    });

    this.route('items.want', {
        path: '/wantlist',
        template: 'main',
        onBeforeAction: function() {
            Session.set('showBack', false);
            Session.set('headerTitle', 'Want List');
            this.next();
        },
        waitOn: function() {
            return Meteor.subscribe('tradeedges');
        },
        data: function() {
            return {
                list: Tradeedges.find({},{   
                    transform: function(doc) {
                        ids = doc._id.split(':');
                        divId = doc._id.replace(':', '');
                        
                        doc.divId =  divId;
                        doc.userItem = Items.findOne({_id: ids[0]});
                        doc.tradeItem = Items.findOne({_id: ids[1]});

                        return doc;
                    }
                })
            }
        },
        controller: 'AppController'
    });

    this.route('items', {
        path: '/items',
        template: 'items',
        onBeforeAction: function() {
            Session.set('showBack', false);
            Session.set('headerTitle', 'Items');
            this.next();
        },
        waitOn: function() {
            return Meteor.subscribe('items', {});
        },
        data: function() {
            return {
                items: Items.find({})
            }
        },
        controller: 'AppController'
    });

    this.route('item.show', {
        path: '/items/:_id',
        template: 'itemDetails',
        onBeforeAction: function() {
            Session.set('showBack', true);
            Session.set('headerTitle', 'Item Details');
            this.next();
        },
        waitOn: function() {
            return Meteor.subscribe("item", this.params._id);
        },
        data: function() {
            var item = Items.findOne({_id: this.params._id});
            return {
                doc: item
            };
        },
        controller: 'AppController'
    });

    /*
    this.route('item.comments', {
        path: '/items/:_id',
        template: 'comments',
        onBeforeAction: function() {
            Session.set('showBack', true);
            Session.set('headerTitle', 'Comments');
            this.next();
        },
        waitOn: function() {
            return Meteor.subscribe("comments", this.params._id);
        },
        data: function() {
            return {
                comments: Comments.find();
            };
        },
        controller: 'AppController'
    });
    */

    this.route('item.add', {
        path: '/addItem',
        template: 'addItem',
        controller: 'AppController',
        onBeforeAction: function(){
            Session.set('showBack', true);   
            this.next(); 
        },
        waitOn: function() {
            return Meteor.subscribe("items", {});
        }
    });

    this.route('profile', {
        path: '/profile',
        controller: 'AppController',
        onBeforeAction: function() {
            Session.set('showBack', false);
            if(!Meteor.userId()) {
                Session.set('intendedRoute', 'profile');
                Session.set('headerTitle', 'Sign In');
                Router.go('login');
            }else{
                Session.set('headerTitle', 'Profile');
                this.next();
            }
        }
    });

    this.route('messages', {
        path: '/messages',
        controller: 'AppController',
        onBeforeAction: function() {
            Session.set('showBack', false);
            if(!Meteor.userId()) {
                Session.set('intendedRoute', 'profile');
                Session.set('headerTitle', 'Sign In');
                Router.go('login');
            }else{
                Session.set('headerTitle', 'Messages');
                this.next();
            }
        }
    });
});