Template['_addImage'].onCreated(function() {
    this.imgSrc = new ReactiveVar('https://placeholdit.imgix.net/~text?txtsize=23&txt=&w=250&h=250');
});

Template['_addImage'].onRendered(function() {
    $('.cloudinary-fileupload').bind('fileuploadprogress', function(e, data) { 
        console.log(JSON.stringify(data));
    });
});

Template['_addImage'].onDestroyed(function() {
    //check that if item not created then delete image from cloudinary
    var notSaved = true;
    var img = this.data.doc.img;
    if(notSaved && img && img._id){
        //cleanupImg(img._id);
    }
});

Template['_addImage'].helpers({
    getImgSrc: function () {
        return Template.instance().imgSrc.get();
    }
});

Template['_addImage'].events({
    "change input[type='file']": function(event, template) {
        $('input[type=submit]').prop("disabled",true);
        $('.ui.dimmer').dimmer('setting', {
        closable: false}).dimmer('show');
        var files;
        var parentData = Template.parentData();
        files = event.currentTarget.files;
        return Cloudinary.upload(files, {
          folder: ''
        }, function(err, res) {
            if(err) {
                console.log("Upload Error: " + err);
            }else {
                var img = template.data.doc.img;
                if(img && img._id) {
                    console.log('deleting img: ', img);
                    console.log(template);
                    cleanupImg(img._id);
                }
                img = {};
                img.res  = res;
                console.log('res from cloudinary: ', res);
                img._id = res.public_id;
                img.url = res.url;
                parentData.doc.img = img;
                var url = "https://res.cloudinary.com/vyndica/image/upload/h_350,w_350,c_fit/" + img._id + "." + res.format;
                template.imgSrc.set(url);
                //parentData.imgSrc.set(res.url);

            }
            $('.ui.dimmer').dimmer('hide');
            $('input[type=submit]').prop("disabled",false);
        });
    }
});
