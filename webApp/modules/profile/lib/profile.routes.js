Router.map(function () {
  this.route('profile', {
    path: '/profile',
    waitOn: function () {
      return Meteor.subscribe('profile')
    },
    onBeforeAction: function () {
      if (!Meteor.user()) {
        Router.go('home')
      } else {
        const profile = Profiles.findOne({userId: Meteor.userId()})
        if (profile) {
          this.next()
        } else {
          Router.go('onboard')
        }
      }
    },
    data: function () {
      return {
        profile: Profiles.findOne({userId: Meteor.userId()}),
        readonly: false
      }
    }
  })

  this.route('settings', {
    name: 'settings',
    path: 'settings',
    onBeforeAction: function () {
      if (!Meteor.user()) {
        Router.go('home')
      }
      this.next()
    }
  })
})
