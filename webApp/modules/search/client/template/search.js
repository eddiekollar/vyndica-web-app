Template['search'].onCreated(function () {})

Template['search'].onRendered(function () {
  var searchTerm = $('#search').val()
  ItemsSearch.search(searchTerm)
})

Template['search'].helpers({
  getItems: function () {
    return ItemsSearch.getData()
  }
})

Template['search'].events({

})
