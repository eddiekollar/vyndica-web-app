Router.map(function () {
  this.route('loop.show',
    {
      path: '/tradeloops/:_id',
      template: 'looplist',
      waitOn: function () {
        return Meteor.subscribe('tradeloop', this.params._id)
      },
      data: function () {
        const _id = this.params._id

        const tradeloop = Tradecycles.findOne({_id: this.params._id})
        const tradeEdgeIds = (tradeloop && tradeloop.tradeEdgeIds) ? tradeloop.tradeEdgeIds : []

        function transform (doc) {
          const ids = doc._id.split(':')
          const divId = doc._id.replace(':', '')

          doc.tradecycleId = _id
          doc.divId = divId
          doc.userItem = Items.findOne({_id: ids[0]})
          doc.tradeItem = Items.findOne({_id: ids[1]})

          return doc
        }

        return {
          title: 'Trade Loop',
          itemTemplate: 'looptradepair',
          tradeloop: tradeloop,
          usersPairs: Tradeedges.find({_id: {$in: tradeEdgeIds}, ownerId: Meteor.userId()}, {
            transform: transform
          }),
          otherPairs: Tradeedges.find({_id: {$in: tradeEdgeIds}, ownerId: {$ne: Meteor.userId()}}, {
            transform: transform
          })
        }
      }
    })

  this.route('loops.show',
    {
      path: '/tradeloops',
      template: 'tradeloops',
      waitOn: function () {
        return Meteor.subscribe('tradeloops')
      },
      data: function () {
        return {
          title: 'Active Trades',
          tradeloops: Tradecycles.find()
        }
      }
    })
})
