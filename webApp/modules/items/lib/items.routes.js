Router.map(function () {
  var subs = new SubsManager()
  this.route('items.show',
    {
      path: '/items',
      template: 'items',
      waitOn: function () {
        return subs.subscribe('items', {})
      },
      data: function () {
        return {
          tems: Items.find({status: {$nin: ['unavailable', 'deleted']}}, {sort: {lastCommentedOn: -1, createdAt: -1}})
        }
      },
      fastRender: true
    })

  // this.route('user.profile')

  this.route('user.items',
    {
      path: '/users/:_id/items',
      template: 'userItems',
      waitOn: function () {
        var params = {}
        if (Meteor.userId() && Meteor.userId() !== this.params._id) {
          params['ownerId'] = {$in: [this.params._id, Meteor.userId()]}
        } else {
          params['ownerId'] = this.params._id
        }
        return subs.subscribe('items', params)
      },
      data: function () {
        return {
          items: Items.find({ownerId: this.params._id}, {sort: {lastCommentedOn: -1, createdAt: -1}}),
          user: Meteor.users.findOne({_id: this.params._id})
        }
      },
      fastRender: true
    })

  this.route('items.mine',
    {
      path: '/myitems',
      template: 'itemsList',
      waitOn: function () {
        const ownerId = Meteor.userId()
        return subs.subscribe('items', {ownerId: ownerId})
      },
      data: function () {
        return {
          title: 'My Items',
          itemTemplate: 'listItem',
          items: Items.find({ownerId: Meteor.userId()})
        }
      },
      fastRender: true
    })

  this.route('items.want',
    {
      path: '/wantlist',
      template: 'itemsList',
      waitOn: function () {
        return subs.subscribe('tradeedges')
      },
      data: function () {
        return {
          title: 'Want List',
          itemTemplate: 'tradepair',
          items: Tradeedges.find({}, {
            transform: function (doc) {
              ids = doc._id.split(':')
              divId = doc._id.replace(':', '')

              doc.divId = divId
              doc.userItem = Items.findOne({_id: ids[0]})
              doc.tradeItem = Items.findOne({_id: ids[1]})

              return doc
            }
          })
        }
      },
      fastRender: true
    })

  this.route('item.add',
    {
      path: '/addItem',
      template: 'newItem',
      waitOn: function () {
        return subs.subscribe('items', {})
      },
      data: function () {
        return {
          doc: {}
        }
      }
    })

  this.route('item.show',
    {
      path: '/items/:_id',
      template: 'item',
      waitOn: function () {
        return subs.subscribe('item', this.params._id)
      },
      onBeforeAction: function () {
        var item = Items.findOne({_id: this.params._id})
        if (!item) {
          Router.go('home')
        } else {
          this.next()
        }
      },
      data: function () {
        return {
          doc: Items.findOne({_id: this.params._id})
        }
      },
      fastRender: true
    })

  /*
  Maybe have endpoint from email redirect to here to ensure security? 
  */
  this.route('item.printlabel', {
    template: 'shippingPrint',
    path: '/items/:_id/shippingprint',
    waitOn: function () {
      return Meteor.subscribe('item', this.params._id)
    },
    onBeforeAction: function () {
      // check for user or email parameter in url

      this.next()
    },
    data: function () {
      return {
        item: Items.findOne({_id: this.params._id})
      }
    },
    layoutTemplate: 'slimLayout'
  })

  this.route('item.graph', {
    path: '/items/:_id/graph',
    template: 'graph',
    waitOn: function () {
      const params = {
        type: 'item',
        selector: {
          _id: this.params._id
        }
      }
      return Meteor.subscribe('graph', params)
    }
  })
})
