Router.map( function () {
    this.route('user.notifications', 
    {
        path: '/users/:_id/notifications',
        template: 'notifications',
        waitOn: function() {
            return Meteor.subscribe('notifications');
        },
        data: function() {
            return {
                title: 'New Activity In:',
                notifications: Notifications.find()
            }
        }
    });
});