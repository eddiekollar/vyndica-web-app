Template.commentsBox.onRendered(function () {
  const container = $('.comment-form')
  container.addClass('custom-comment-form')
  container.removeClass('comment-form')
})

Template.commentsBox.events({
  'submit .custom-comment-form': function (event) {
    const eventScope = this
    const container = $(event.target).parent()

    if (event.type === 'submit') {
      event.preventDefault()

      if (Meteor.user()) {
        let textarea = container.find('.create-comment')
        let value = textarea.val().trim()

        Comments.addCustom(eventScope.id, eventScope.collectionName, value)
        textarea.val('')
      } else {
        Comments.session.set('loginAction', 'add comments')
      }
    }
  }
})
