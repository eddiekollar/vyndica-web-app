Template['notifications'].events({
    'click tr.clickable': function (event, template) {
        Router.go('item.show', {_id: event.target.id});
    }
});