Meteor.startup(function () {
    Accounts.urls.resetPassword = function(token) {
      return Meteor.absoluteUrl('reset-password/' + token);
    };
    
    var smtp = Meteor.settings.smtp;

    Accounts.emailTemplates.siteName = 'Vyndica';
    Accounts.emailTemplates.from = 'Vyndica <hello@vyndica.com>';

    process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;

    Mandrill.config({
      username: Meteor.settings.MANDRILL.MANDRILL_API_USER,  // the email address you log into Mandrill with. Only used to set MAIL_URL.
      key: Meteor.settings.MANDRILL.MANDRILL_API_KEY,  // get your Mandrill key from https://mandrillapp.com/settings/index
      port: 587,  // defaults to 465 for SMTP over TLS
      // host: 'smtp.mandrillapp.com',  // the SMTP host
      // baseUrl: 'https://mandrillapp.com/api/1.0/'  // update this in case Mandrill changes its API endpoint URL or version
    });
});