function loadFixture(fixtures, collection) {
  var i;

  for (i = 0; i < fixtures.length; i+= 1) {
    //collection.remove({ });
    collection.insert(fixtures[i]);
  }
}

Meteor.startup(function () {
  //loadFixture(Fixtures['dummyFixture'], DummyCollection);
  var docs = YAML.eval(Assets.getText('data/interests.yml'));

  for (key in docs) if (docs.hasOwnProperty(key)) {
    var doc = docs[key];

    var alreadExists = typeof Interests.findOne({title: doc.title}) === 'object';
    if(!alreadExists) {
        doc.createdAt = Date.now();
        Interests.insert(doc); 
    }
  }
});
