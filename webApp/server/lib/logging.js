Meteor.startup(function () {
    var env = Meteor.settings.public.environment;

    log = Winston;

    if(env === 'staging' || env === 'production') {
        log.add( Winston_Papertrail, {
        levels: {
            debug: 0,
            info: 1,
            warn: 2,
            error: 3,
            auth: 4
        },
        colors: {
            debug: 'blue',
            info: 'green',
            warn: 'red',
            error: 'red',
            auth: 'red'
        },

        host: "logs3.papertrailapp.com",
        port: 38332, 
        handleExceptions: true,
        json: true,
        colorize: true,
        logFormat: function(level, message) {
            return '[' + env + '] ' + level + ': ' + message;
        }
        });
    }else {
        //setup logging for local
    }
});