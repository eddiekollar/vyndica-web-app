BrowserPolicy.content.allowInlineScripts()
BrowserPolicy.content.allowFontDataUrl()

var trusted = [
  '*.googleapis.com',
  '*.gstatic.com',
  '*.bootstrapcdn.com',
  '*.placehold.it',
  '*.imgix.net',
  '*.cloudinary.com',
  '*.gravatar.com',
  'i1.wp.com',
  '*.hotjar.com',
  'connect.facebook.net',
  '*.facebook.com',
  '*.amazonaws.com',
  '*.zoomcharts-cloud.com',
  '*.braintreegateway.com',
  '*.nudgespot.com',
  '*.pusher.com'
]

_.each(trusted, function (origin) {
  BrowserPolicy.content.allowOriginForAll(origin)
})
