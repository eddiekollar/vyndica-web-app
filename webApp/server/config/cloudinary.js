Cloudinary.config({
    cloud_name: Meteor.settings.CLOUDINARY.CLOUD_NAME,
    api_key: Meteor.settings.CLOUDINARY.API_KEY,
    api_secret: Meteor.settings.CLOUDINARY.API_SECRET
});