Accounts.onCreateUser(function(options, user) {
    //Set profile setup state
    console.log(JSON.stringify(options));

    options.profile = options.profile || {};

    options.profile.setup = false;

    user.profile = options.profile;

    if(Meteor.settings.public.environment === 'production') {
        var email = options.email;
        var campaignId =  Meteor.settings.public.kickofflabs.campaignId;

        var postOptions = {
            params: {
                email: email
            }
        };

        HTTP.post('https://api.kickofflabs.com/v1/' + campaignId + '/subscribe', postOptions, 
            function (error, result) {
                if(error) {
                    console.error('kickofflabs error: ', error);
                }else{
                    console.log('kickofflabs: ', JSON.stringify(result));
                }
        });
    }

    return user;
});

Meteor.methods({
    "userExists": function(username){
        check(username, String);
        return !!Meteor.users.findOne({username: username});
    },
});

Meteor.methods({
    "emailExists": function(email, args){
        check(args, Match.Any);
        check(email, String);
        return !!Meteor.users.findOne({emails: {$elemMatch: {'address': email}}});
    },
});