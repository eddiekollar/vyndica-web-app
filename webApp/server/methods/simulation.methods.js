/*
    Generate simulated users, items, and graph
*/

var getParagraph = function(){
    HTTP.get('https://baconipsum.com/api/?type=all-meat&paras=1&start-with-lorem=0',{},function(error, response) { 
            if(error)
                console.error(error);
            console.log(JSON.stringify(response.data[0]));
        });
}

Meteor.methods({
    generateData: function(numUsers) {
        check(numUsers, Number);
        var env = Meteor.settings.public.environment;
        if(env === 'development' || env === 'staging') {
            HTTP.get('http://api.randomuser.me/?results=' + numUsers + '&nat=us', {}, function (error, response) {
                if (!error) {
                    console.log('Received ' + response.data.results.length + ' random users');
    
                    _.each(response.data.results, function(randUser) {
                        randUser = randUser.user;
                        console.log('random user: ', randUser);
                        var user = Meteor.users.findOne({username: randUser.username});
                        console.log('returned user object: ', user);
    
                        if(!user) {
                            user = {};
                            user.username = randUser.username;
                            user.email = randUser.email;
                            user.password = randUser.password;
    
                            user.profile = {
                                isRandom: true
                            };

                            var userId = Meteor.users.insert(user);

                            profile = {}
                            profile.firstName = randUser.name.first;
                            profile.lastName = randUser.name.last;
                            profile.street1 = randUser.location.street;
                            profile.city = randUser.location.city;
                            profile.zip = randUser.location.zip;
                            profile.state = randUser.location.state;
                            profile.userId = userId;
                            profile.isRandom = true;

                            Profiles.insert(profile);
    
                            console.log('Created new user: ', JSON.stringify(user), JSON.stringify(profile));
                        }
                    });
                }else{
    
                    console.error(error);
                }
            });
        }

            //Call API for random stuff

            //Generate graph
    },
    generateParagraph: function() {
        getParagraph();
    }
});