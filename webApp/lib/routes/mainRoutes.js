// Home Route
// var subscriptions = new SubsManager()

Router.map(function () {
  var subs = new SubsManager()
  this.route('/', {
    name: 'home',
    path: '/',
    onBeforeAction: function () {
      var user = Meteor.user()
      if (user) {
        if (!user.profile.setup) {
          // Router.go('onboard') //Go to profile setup
        }
      }
      this.next()
    },
    waitOn: function () {
      return subs.subscribe('items', {})
    },
    data: function () {
      return {
        items: Items.find({status: {$nin: ['unavailable', 'deleted']}}, {sort: {lastCommentedOn: -1, createdAt: -1}})
      }
    },
    fastRender: true
  })

  this.route('search', {
    template: 'search',
    path: '/search',
    waitOn: function () {
      return subs.subscribe('items', {})
    },
    fastRender: true
  })

  this.route('terms', {
    template: 'terms',
    path: '/terms'
  })

  this.route('privacy', {
    template: 'privacy',
    path: '/privacy'
  })

  this.route('about', {
    template: 'about',
    path: '/about'
  })

  this.route('shippinglabel', {
    template: 'shippingPrint',
    path: '/shippingprint',
    layoutTemplate: 'slimLayout'
  })

  this.route('onboard', {
    name: 'onboard',
    path: '/onboard',
    waitOn: function () {
      return subs.subscribe('interests')
    },
    data: function () {
      return {interests: Interests.find()}
    },
    onBeforeAction: function () {
      if (!Meteor.user()) {
        Router.go('home')
      } else {
        var user = Meteor.user()
        if (user.profile.setup) {
          // Router.go('home')
        }
      }
      this.next()
    }
  })

  this.route('graph', {
    name: 'graph',
    path: '/graph',
    onBeforeAction: function () {
      const user = Meteor.user()
      if (!user || (!Roles.userIsInRole(user._id, 'admin', Roles.GLOBAL_GROUP))) {
        Router.go('home')
      }
      this.next()
    },
    waitOn: function () {
      const params = {
        type: 'admin',
        selector: {}
      }
      return Meteor.subscribe('graph', params)
    }
  })

  this.route('user.graph', {
    path: '/users/:_id/graph',
    template: 'graph',
    waitOn: function () {
      const params = {
        type: 'user',
        selector: {
          ownerId: this.params._id
        }
      }
      return Meteor.subscribe('graph', params)
    }
  })
})

/*
Pages = new Meteor.Pagination(Items, {
  itemTemplate: "Item", // add your template's name here
  route: "/",
  router: "iron-router",
  routerTemplate: "Items",
  routerLayout: "Layout",
  sort: {
    id: 1
  },
  templateName: "Items"
})
*/
