Router.configure({
  layoutTemplate: 'basicLayout',
  notFoundTemplate: 'notFound',
  loadingTemplate: 'spiralLoading'
});