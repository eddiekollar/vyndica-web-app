Template.graph.onRendered(function () {
  let self = this
  self.menuBody = undefined

  let t = new NetChart({
    container: document.getElementById('graph'),
    area: { height: 500 },
    events: {
      onClick: graphClick,
      onPositionChange: (event) => {
        hidePopup()
      }
    },
    data: { dataFunction: function (nodeList, success, error) {
        let links = []
        let nodes = []
        let itemIds = []

        Tradeedges.find().forEach(function (edge) {
          let ids = edge._id.split(':')

          let itemsExist = true
          _.each(ids, function (id) {
            if (itemIds.indexOf(id) < 0) {
              const item = Items.findOne({_id: id})

              if (item) {
                let style = {'label': item.title, 'image': item.imgURL(CLOUDINARY_IMG_THUMB_PARAMS)}

                if ((Router.current().route.getName() === 'user.graph' && item.ownerId === Router.current().params._id) ||
                  (Router.current().route.getName() === 'item.graph' && item._id === Router.current().params._id)) {
                  style.lineColor = '#0000FF'
                  style.lineWidth = 7
                }

                nodes.push({'id': item._id, 'loaded': true, style: style})
                itemIds.push(id)
              } else {
                itemsExist = false
              }
            }
          })

          if (itemsExist) {
            links.push({ 'id': edge._id, 'from': ids[0], 'to': ids[1], style: {'toDecoration': 'arrow'}})
          }
        })

        success({ 'nodes': nodes, 'links': links })
    } },
    navigation: {
      mode: 'showall'
    },
    layout: {
      nodeSpacing: 30
    },
    style: {
      node: { imageCropping: true },
      linkStyleFunction: linkStyle
    }
  })

  let menuElement = document.getElementById('custom_context_menu')

  document.body.appendChild(menuElement)

  function graphClick (event, args) {
    console.log(event, args)
    if (args.clickNode) {
      const item = Items.findOne({_id: args.clickNode.id})

      menuElement.style.display = 'block'

      let menuClose = document.getElementById('close_menu')
      menuClose.addEventListener('mousedown', hidePopup)

      self.menuBody = Blaze.renderWithData(Template.menuBody, item, menuElement)

      const maxLeft = $(window).width() - $(menuElement).width()
      const maxTop = $(window).height() - $(menuElement).height()

      const left = (maxLeft < event.pageX) ? maxLeft : event.pageX
      const top = (maxTop < event.pageY) ? maxTop : event.pageY

      menuElement.style.left = left + 'px'
      menuElement.style.top = top + 'px'
    }
    event.preventDefault()
  }

  function hidePopup () {
    if (self.menuBody) {
      Blaze.remove(self.menuBody)
    }
    menuElement.style.display = 'none'
  }

  // hide the popup whenever the mouse is clicked outside the chart
  // window.addEventListener('mousedown', hidePopup)

  // the chart captures all mouse events so the workaround is to subscribe to the mouse event within the chart
  var chartInteraction = document.getElementsByClassName('DVSL-interaction')[0]
  chartInteraction.addEventListener('mousedown', hidePopup)
  chartInteraction.addEventListener('pointerdown', hidePopup)
  window.addEventListener('resize', hidePopup)

  function disposeDemo () {
    // function should be called whenever the chart is removed
    window.removeEventListener('mousedown', hidePopup)
    chartInteraction.removeEventListener('mousedown', hidePopup)
    chartInteraction.removeEventListener('pointerdown', hidePopup)
    menuElement.remove()
    disposeDemo = null
  }

  function linkStyle (link) {
    link.fillColor = 'limegreen'
  }
})

Template.graph.helpers({
  hasData: function () {
    return Tradeedges.find().count() > 0
  },
  routeName: function () {
    return Router.current().route.getName()
  }
})

Template.menuBody.events({
  'click [data-action=view-item]': function (event, template) {
    Router.go('item.show', {_id: template.data._id})
  }
})
