Template.header.onCreated(function () {
  Session.set('isActive', false)

  var self = this
  self.notificationsCount = new ReactiveVar(0)

  self.authState = new ReactiveVar('signUp')

  self.autorun(function () {
    var subscription = self.subscribe('notifications')

    if (subscription.ready()) {
      self.notificationsCount.set(Notifications.find().count())
    }
  })

  self.autorun(function () {
    const user = Meteor.user()
    if (user) {
      const profile = Profiles.findOne({userId: user._id})
      let userData = {}

      if (profile) {
        userData['first_name'] = profile.firstName
        userData['last_name'] = profile.lastName
      }
      nudgespot.identify(user.emails[0].address, userData)
    }
  })

  if (!Meteor.userId()) {
    $('.header-wrapper').css('background', 'rgba(0,0,0,0)')
    $('.header-wrapper .item').addClass('invert')
    $(window).scroll(function () {
      if ($(window).scrollTop() > 450) {
        $('.header-wrapper').removeClass('clear')
        $('.header-wrapper .item').removeClass('invert')
      } else {
        $('.header-wrapper').addClass('clear')
        $('.header-wrapper .item').addClass('invert')
      }
    })
  }
})

Template.header.onRendered(function () {
  $('.ui.dropdown').dropdown()
})

Template['header'].helpers({
  isActive: function () {
    return Session.get('isActive') ? 'active' : ''
  },
  animateClass: function () {
    return Session.get('isActive') ? 'fadeIn' : 'fadeOut'
  },
  iconClass: function () {
    return Meteor.user() ? 'user' : 'sign in'
  },
  iconText: function () {
    return Meteor.user() ? 'Log Out' : 'Sign In'
  },
  isOnboarding: function (path) {
    return path === '/onboard'
  },
  getNotificationsCount: function () {
    return Template.instance().notificationsCount.get()
  },
  getAuthState: function () {
    return Template.instance().authState.get()
  }
})

Template['header'].events({
  'keypress #search': function (event, template) {
    if (event.keyCode === 13) {
      var text = $(event.target).val().trim()
      ItemsSearch.search(text)
      if (Iron.Location.get().path !== '/search') {
        Router.go('search')
      }
    }
  },
  'click .search.link.icon': function (event, template) {
    var text = $('#search').val().trim()
    ItemsSearch.search(text)
    if (Iron.Location.get().path !== '/search') {
      Router.go('search')
    }
  },
  'click .right.menu.open': function (e) {
    e.preventDefault()
    $('.ui.vertical.menu').toggle()
  },
  'click #addItem': function () {
    Router.go('item.add')
  },
  'click .signin': function (event, template) {
    Session.set('isActive', !Session.get('isActive'))
    if (!Meteor.user()) {
      template.authState.set('signIn')
      $('#auth-modal').modal('show')
    } else {
      Meteor.logout()
    }
  },
  'click .signup': function (event, template) {
    template.authState.set('signUp')
    $('#auth-modal').modal('show')
  },
  'click .item.mine': function (event, template) {
    Router.go('items.mine')
  },
  'click .item.want': function (event, template) {
    Router.go('items.want')
  },
  'click .notifications': function (event, template) {
    Router.go('user.notifications', {_id: Meteor.user()._id})
  }
})
