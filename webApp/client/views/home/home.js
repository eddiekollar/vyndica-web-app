Template.home.helpers({
});

Template.home.events({
});


Template.home.rendered = function () {
  // @see: http://stackoverflow.com/questions/5284814/jquery-scroll-to-div
  $('a[href*=#]:not([href=#])').click(function () {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }

    return true;
  });

  if(!Meteor.user()) {
    /*
    Meteor.call('hasSeenWelcome', function(error, result) {
      if(error || !result) {
        $('#welcome').modal('show');
      }
    });
  */
  //$('#welcome').modal('show');
  }
};
