Template.basicLayout.helpers({
  getRouteName: function () {
    return Router.current().route.getName()
  }
})

Template.basicLayout.events({
  'click [data-action=signup]': function () {
    $('#auth-modal').modal('show')
  }
})
