Template.onboard.rendered = function () {
  $('.ui.checkbox').checkbox()
}

Schemas.interests = new SimpleSchema({
  'interests': {
    type: [String],
    optional: true
  }
})

Template['onboard'].helpers({
  getInterestSize: function () {
    console.log('onboard: ', this)
  },
  steps: function () {
    return [{
      id: 'profile',
      title: 'Personal Information',
      formId: 'information-form',
      template: 'information',
      schema: Schemas.profile
    }, {
      id: 'interests',
      title: 'Interests',
      formId: 'interests-form',
      template: 'interests',
      schema: Schemas.interests,
      onSubmit: function (data, wizard) {
        var profileData = wizard.mergedData()
        profileData.userId = Meteor.userId()
        console.log('data: ', data)
        console.log('profileData: ', profileData)
        var checked = $('[type="checkbox"]:checked')
        var interests = []

        _.each(checked, function (elem) {
          interests.push(elem.value)
        })

        profileData.interests = interests

        delete profileData.insertDoc
        delete profileData.updateDoc

        Meteor.call('createUserProfile', profileData, function (err, res) {
          if (err) {
            console.error('createUserProfile error: ', err)
          } else {
            console.log('createUserProfile success')

            Router.go('home')
          }
        })
      }
    }]
  }
})

Template.information.onRendered(function () {
  $('nav').append('<button type="button" class="wizard-skip-button ui button">Skip</button>')
})

Template.interests.onRendered(function () {
  $('nav').append('<button type="button" class="wizard-skip-button ui button">Skip</button>')
})

Template.information.events({
  'click .wizard-skip-button': function (event, template) {
    event.preventDefault()
    Router.go('home')
  }
})

Template['interests'].events({
  'click .submit.button': function (event, template) {
    console.log('clicking submit')
    event.preventDefault()

  // Meteor.users.update( { _id: Meteor.userId() }, { $set: { 'profile.setup': true, 'profile.interests': interests }} )
  },
  'click .wizard-skip-button': function (event, template) {
    event.preventDefault()
    Router.go('home')
  },
  'click .wizard-submit-button': function (event, template) {
    console.log('clicking confirm')
    // event.preventDefault()
    $('#information-form').form({
      fields: {
        firstName: 'empty',
        lastName: 'empty',
        street1: 'empty',
        city: 'empty',
        state: 'empty',
        zip: 'empty'
      }
    })
  }
})
