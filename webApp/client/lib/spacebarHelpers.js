Meteor.startup(function () {
  Session.set('isMobile', $(window).width() <= 768)
  $(window).resize(function (evt) {
    Session.set('isMobile', $(window).width() <= 768)
  })
})

Template.registerHelper('debug', function (optionalValue) {
  if (typeof console !== 'undefined' || typeof console.log !== 'undefined') {
    console.log('Current Context')
    console.log('====================')
    console.log(this)
    if (optionalValue) {
      console.log('Value')
      console.log('====================')
      console.log(optionalValue)
    }

    return ''
  }

  // For IE8
  alert(this)

  if (optionalValue) {
    alert(optionalValue)
  }

  return ''
})

Template.registerHelper('constant', function (what) {
  return Meteor.App[what.toUpperCase()]
})

Template.registerHelper('getCurrentPath', function () {
  return Iron.Location.get().path
})

Template.registerHelper('isDocOwner', function () {
  return (this.doc && (Meteor.userId() === this.doc.ownerId))
})

Template.registerHelper('isMobile', function () {
  return Session.get('isMobile')
})

Template.registerHelper('itemsIndex', function () {
  return ItemsIndex
}); // instanceof EasySearch.Index)

Template.registerHelper('getImgParams', function () {
  return CLOUDINARY_IMG_THUMB_PARAMS
})
