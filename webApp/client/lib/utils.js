cleanupImg = function(public_id) {
    Cloudinary.delete(public_id, function(err, res){
        if(!err) {
            console.log("deleted file: ", res);
        }else{
            console.log("error deleting file: ", err);
        }
    });
};